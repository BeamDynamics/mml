PORTING THE WINDOWS VERSION OF ACCELERATOR TOOLBOX TO UNIX

I assume you are familiar with mex-files in MATLAB.
I suggest you also install AT on a Windows 
machine as a reference.

BACKGROUND (See also 'MATLAB API Reference Manual'):

1. mex-file is a piece of an object code that MATLAB can load
   and execute.

2. User-written C files must have MATLAB-specific interface:
   They must implement a function 'mexFunction()' which serves
   as an enty point or gateway between the mex-file and MATLAB.

3. The easiest (and sufficient) way to make mex-files is to 
   use MATLAB command 'mex'.
   Example: If myfun.c is a file that contains 
   the mexFunction definition, the following 
   command builds the mex-file:

   >> mex myfun.c
   
   Try also:
 
   >> help mex 
   
   to see more options and some platform specific issues

4. The mex-file produced by 'mex' command is PLATFORM-SPECIFIC
   It even has different extensions:
   .dll      on Windows
   .mexglx   on Linux

5. If C file uses only PORTABLE C CODE - porting is trivial:
   Copy the C file to a new platform and let the 'mex' command 
   take care of it.
   
6. If some portions are not portable, the possible solutions are
   A: modify C file to compile and run only under the 
      new platform
   B:(better) Make platform-specific compilation branches 
      in the C code using #ifdef compiler directive 


THE USE OF MEX-FILES IN ACCELERATOR TOOLBOX

1. There are so called 'pass-methods' 
   located in  ATROOT/simulator/element
   For example: 
   DriftPass.c
   QuadLinearPass.c
   ...
   THESE ARE PLATFORM DEPENDENT but easy to port:
   
   1.1 Each of these functions has a platform-INdependent interface
       mexFunction() (Examine DriftPass.c for example)
       It allows the function to be called from command line:
       >> D.Length = 1;
       >> DriftPass(D,[0.01 0 0 0 0 0]')

   1.2 A typical mex-file (See MATLAB API reference manual)
       is a shared library that exports only one function - mexFunction().
       
       ( On Windows NT the list of exported functions of a.dll file
         can be viewed with right-click, QuickView. I think 'nm' 
         does the same on linux ) 
         
       In Accelerator Toolbox in order to achieve maximum speed
       for multi-turn tracking, pass-methods also export
       a passFuntion(). This allows other mex-files to load and 
       execute pass-methods directly.
       ( See the prototype of passFunction 
         in ATROOT/simulator/element/elempass.h)

   1.3 Compiling pass-methods on a different platform.
       All I had to do was to modify the declaration
       of passFunction in one header file: elempass.h
       used by all pass-methods
       
       on Windows
       #define ExportMode __declspec(dllexport)
       
       I tried on Linux and True 64 Unix: define ExportMode to nothing
       #define ExportMode
       
   1.4 File name convention. 

       I realized after AT1.0 that
       MATLAB on UNIX is case sensitive. It is inconvenient
       to type commands with upper case characters
       from MATLAB command prompt. However, upper case is
       great for naming pass-methods (StrMPoleSymplectic4Pass)
       Beginning in AT1.1,
       ONLY pass-methods mex-file names should contain upper case
       All other filenames (.m and mex-files) should be lowercase
      

2. There is a group of functions for tracking intended for a 

   small number of turns. Located in ATROOT/simulator/track
   ringpass.c
   linepass.c
   
   These are trivial to port because they use only PORTABLE C
   on the new platform should do it   


   >>mex ringpass.c

   They do NOT call pass-methods directly but
   through MATLAB API function 'mexCallMATLAB'
   Therefore if a pass-method works from command line
   as in the example above:

   >> DriftPass(D,[0.01 0 0 0 0 0]')

   ringpass should be able to call it.
        

3. Functions that track particles through the ring
   for LARGE number of turns 
   located in ATROOT/simulator/track
   
   ringpassw.c - 'w' for windows
   ringpassl.c - 'l' for linux 
   ...
   These are OPTIMIZED for speed on specific platform.
   Unlike 'ringpass' or 'linepass'  in 2. they load 
   pass-methods as dynamic shared libraries and
   execute passFunction.

   These require SOME WORK to port.

   3.1. Calling a function in a shared library is a feature
        provided by the operating system. 
        There is almost a one-to-one correspondence
        of C functions in system libraries.
   
        Windows               |  Linux
        ______________________|____________________
        #include <windows.h>  |  #include <dlfcn.h>
        ______________________|____________________
        LoadLibrary()         |  dlopen()
        GetProcAddress()      |  dlsym()
        FreeLibrary()         |  dlclose()


4. In future, as the number of functions in AT increases, some 
   physics functions/tools  (other than categories 1,2 above) 
   might gain in speed tremendously, if written in C instead of 
   MATLAB language. 
   
   AT philosophy:  
   a. The number of these should be kept to MINIMAL 
   b. They shoul be self-contained 
      (single C source file for each mex-file)
   c. They should be written in PORTABLE C
   
   For example:
   In AT1.1 (coming in June for PAC 2001 conference)
   there is a findmpoleraddiffmatrix.c
   that calculates Ohmi's radiation diffusion matrix
   for equilibrium beam envelope calculations