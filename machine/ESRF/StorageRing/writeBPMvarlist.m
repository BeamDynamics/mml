function [varlist] = writeBPMvarlist(plane)


celllist = 1:32;
celllist = [celllist(4:end) celllist(1:3)];

varlist = {};
ind = 1;
for cell=celllist
    for bpmNum=1:10
        newarr = {ind [cell bpmNum] ['srdiag/bpm/c' sprintf('%02d', cell) '-' sprintf('%02d', bpmNum)] 1 [plane 'BPM_C' sprintf('%02d', cell) '-' sprintf('%02d', bpmNum)]};
        varlist = [varlist; newarr];  
        ind = ind+1;
    end
end

end
