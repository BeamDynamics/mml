function [ErrorFlag] = setvst(varargin)
%GETHBPMAVERAGE - Gets horizontal orbit read into valid BPMS 
%
%  INPUTS
%  1. Familyname
%  2. Field
%  3. DeviceList - BPM devicelist
%  4. time
%
%  OUTPUTS
%  1. AM - horizontal beam position
%
%  NOTES
%  First shot
%
%  See Also gethbpmgroup, gethbpmmanager


%
% Written by Laurent S. Nadolski

t0 = clock;  % starting time for getting data
DataTime = 0;
ErrorFlag = 1;
Field = 'Monitor';
DeviceListTotal = family2dev('VCOR');

if length(varargin) < 3
    error('wrong argument')
else 
    SPvalue = varargin{3};
    if length(varargin) < 4
        DeviceList = DeviceListTotal;
    else
        DeviceList = varargin{4};
    end
end

SPread = getvst;

Status = findrowindex(DeviceList, DeviceListTotal);

SPread(Status) = SPvalue;

tango_write_attribute2('srmag/vst/all', 'Strengths',SPread');
