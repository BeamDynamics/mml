function aoinit(SubMachineName)
%AOINIT - Initialization function for the Matlab Middle Layer (MML)
%
%  See Also esrfinit, setpathesrf, updateatindex, setoperationalmode

% Written by Laurent S. Nadolski

if exist('SubMachineName', 'var') && ~strcmpi(SubMachineName, 'StorageRing')
    error('Wrong SubMachine %s', SubMachineName)
end

[~, WHO] = system('whoami');
% system gives back an visible character: carriage return!
% so comparison on the number of caracters

% The path should not be modified in standalone mode
if ~isdeployed_local

    MMLROOT = getmmlroot;
    %MMLDATAROOT = getmmldataroot;
    
    % Make sure mml is high on the path
    % why is it important?
    % August Test at this place in order not to shadow new BBA
    addpath(fullfile(MMLROOT, 'mml'),'-begin');

    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing'));
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'bpm'));
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'magnet'));
%     addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'quad'));
%     addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'tune'));
%     addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'feedforward'));
%     addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'Operation'));
%         
    %addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'insertions','proc_emphu'));
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'loco'));
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'orbit'));
    
    % for operational data
    addpath(fullfile(getmmlconfigroot, 'machine', 'ESRF', 'StorageRingOpsData'));
    
    % APPLICATIONS
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing', 'Lattices'));
    % A. Madur BBA Application
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing','BBA'));
    
    % MML tune BBA application
    addpath(fullfile(MMLROOT, 'machine', 'ESRF', 'StorageRing','bba_mml'));
    addpath(fullfile(MMLROOT, 'mml', 'setorbitbumpgui'));        
    addpath(fullfile(MMLROOT, 'mml', 'setorbitgui'));        
    
    disp('Startup file for Matlab Middle Layer read with success');

end

% Initialize the MML for machine

esrfinit;

function RunTimeFlag = isdeployed_local
% isdeployed is not in matlab 6.5
V = version;
if str2double(V(1,1)) < 7
    RunTimeFlag = 0;
else
    RunTimeFlag = isdeployed;
end

