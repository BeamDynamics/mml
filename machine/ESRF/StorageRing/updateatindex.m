function updateatindex(THERING)
%UPDATEATINDEX - Updates the AT indices in the MiddleLayer with the present AT lattice (THERING)

%
% Adapted by Laurent S. Nadolski
% Modified 21 November Nanoscopium S11/S12 missing Atgroupparameter

if nargin == 0 
    global THERING
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Append Accelerator Toolbox information %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Since changes in the AT model could change the AT indexes, etc,
% It's best to regenerate all the model indices whenever a model is loaded

% Sort by family first (findcells is linear and slow)
Indices = atindex(THERING);

AO = getao;

%% BPMS
try
    ifam = 'BPMx';
    AO.(ifam).AT.ATType = 'X';
    AO.(ifam).AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.(ifam).Position = findspos(THERING, AO.(ifam).AT.ATIndex)';

    ifam = 'BPMy';
    AO.(ifam).AT.ATType = 'Y';
    AO.(ifam).AT.ATIndex = Indices.BPM(:); % findcells(THERING,'FamName','BPM')';
    AO.(ifam).Position = findspos(THERING, AO.(ifam).AT.ATIndex)';
catch err
    warning('MML:wrongfamily', '%s family not found in the model.', ifam);
    fprintf('Message: %s\n', err.message);
end


%% CORRECTORS
try    
   
    %% Horizontal correctors are at every AT corrector
    ifam = 'HCOR';
    AO.(ifam).AT.ATType = ifam;
   
    AO.(ifam).AT.ATIndex = getHCORinds;
        
    AO.(ifam).Position = findspos(THERING, AO.(ifam).AT.ATIndex(:,1))';

    %% Vertical correctors are at every AT corrector
    ifam = 'VCOR';
    AO.(ifam).AT.ATType = ifam;

    AO.(ifam).AT.ATIndex = getVCORinds;

    AO.(ifam).Position = findspos(THERING, AO.(ifam).AT.ATIndex(:,1))';

%     if CorrInSextup
%         AO.(ifam).AT.ATIndex = buildatindex(AO.(ifam).FamilyName, [Indices.SextCOR(1:30)'; Indices.VCMHU640(:); Indices.SextCOR(31:end)']);
%     end
    
catch err
    warning('MML:wrongfamily', 'Corrector family %s not found in the model.',ifam);
    fprintf('Message: %s\n', err.message);
end

%% QUADRUPOLES

%% RF CAVITY
try
    AO.RF.AT.ATType = 'RF Cavity';
    AO.RF.AT.ATIndex = findcells(THERING,'Frequency')';
    AO.RF.Position = findspos(THERING, AO.RF.AT.ATIndex(:,1))';
catch err
    warning('MML:wrongfamily', 'RF cavity not found in the model.');
    fprintf('Message: %s\n', err.message);
end

setao(AO);

%% Set TwissData at the start of the storage ring
try

    % BTS twiss parameters at the input
    TwissData.alpha = [0 0]';
    TwissData.beta  = [13.8467 2.2582]';
    TwissData.mu    = [0 0]';
    TwissData.ClosedOrbit = [0 0 0 0]';
    TwissData.dP = 0;
    TwissData.dL = 0;
    TwissData.Dispersion  = [.06 0 0 0]';

    setpvmodel('TwissData', '', TwissData);  % Same as, THERING{1}.TwissData = TwissData;

catch err
    warning('MML:wrongfamily','Setting the twiss data parameters in the MML failed.');
    fprintf('Message: %s\n', err.message);
end
