function [hcorInds] = getHCORinds

    global THERING
    idq1=    findcells(THERING, 'FamName', 'DQ1\w*');
    LDQ1=getcellstruct(THERING,'Length',idq1);
    
    idq1=idq1(LDQ1>0.8);

    iother=findcells(THERING, 'FamName', 'S[HFDIJ]\w*','DQ2C_2');

    hcorInds=sort([idq1,iother]);
    
    hcorInds = hcorInds(:);
    
    
end