function esrfinit(OperationalMode)
%ESRFINIT - Initializes params for ESRF control in MATLAB
%
% Written by Laurent S. Nadolski, Synchrotron SOLEIL
%
%==========================
% Accelerator Family Fields
%==========================
% FamilyName            BPMx, HCOR, etc
% CommonNames           Shortcut name for each element
% DeviceList            [Sector, Number]
% ElementList           number in list
% Position              m, if thick, it is not the magnet center
%
% MONITOR FIELD
% Mode                  online/manual/special/simulator
% TangoNames            Device Tango Names
% Units                 Physics or HW
% HW2PhysicsFcn         function handle used to convert from hardware to physics units ==> inline will not compile, see below
% HW2PhysicsParams      params used for conversion function
% Physics2HWFcn         function handle used to convert from physics to hardware units
% Physics2HWParams      params used for conversion function
% HWUnits               units for Hardware 'A';
% PhysicsUnits          units for physics 'Rad';
% Handles               monitor handle
%
% SETPOINT FIELDS
% Mode                  online/manual/special/simulator
% TangoNames            Devices tango names
% Units                 hardware or physics
% HW2PhysicsFcn         function handle used to convert from hardware to physics units
% HW2PhysicsParams      params used for conversion function
% Physics2HWFcn         function handle used to convert from physics to hardware units
% Physics2HWParams      params used for conversion function
% HWUnits               units for Hardware 'A';
% PhysicsUnits          units for physics 'Rad';
% Range                 minsetpoint, maxsetpoint;
% Tolerance             setpoint-monitor
% Handles               setpoint handle
%
%=============================================
% Accelerator Toolbox Simulation Fields
%=============================================
% ATType                Quad, Sext, etc
% ATIndex               index in THERING
% ATParamGroup      param group
%
%============
% Family List
%============
%    BPMx
%    BPMz
%    HCOR
%    VCOR
%    BEND
%    Q1 to Q10
%    S1 to S10
%    RF
%    TUNE
%    DCCT
%    Machine Params
%
% NOTES
%   All sextupoles have H and V corrector and skew quadrupole windings
%
%  See Also setpathsoleil, setpathmml, aoinit, setoperationalmode, updateatindex

%
%
% TODO, Deltakick for BPM orbit response matrix  Warning optics dependent cf. Low alpha lattice
%       to be put into setoperationalmode

% DO NOT REMOVE LSN
%suppress optimization message
%#ok<*ASGLU>

% CONTROL ROOM
% Check for nanoscopium
% Check for attribute names
% Check for range value of sextupoles

% If controlromm user is operator and online mode

[statuss WHO] = system('whoami');
% system gives back an visible character: carriage return!
% so comparison on the number of caracters
if strncmp(WHO, 'operateur',9),
    ControlRoomFlag = 1;
    Mode = 'Online';
else
    ControlRoomFlag = 0;
    Mode = 'Simulator';
end

%% Default operation mode (see setoperationalmode)
if nargin < 1
    OperationalMode = 1;
end

% Define some global variables

h = waitbar(0,'esrfinit initialization, please wait');

%==============================
%% load AcceleratorData structure
%==============================

setad([]);       %clear AcceleratorData memory
AD.SubMachine = 'StorageRing';   % Will already be defined if setpathmml was used
AD.Energy        = 6.000; % Energy in GeV needed for magnet calibration. Do not remove!

setad(AD);

%%%%%%%%%%%%%%%%%%%%
% ACCELERATOR OBJECT
%%%%%%%%%%%%%%%%%%%%

if ~isempty(getappdata(0, 'AcceleratorObjects'))
    AO = getao;
    % Check if online and AO is from Storagering
    if ControlRoomFlag && isfield(AO, 'Q10') 
        local_tango_kill_allgroup(AO); % kill all TANGO group
    end
end
AO =[]; setao(AO);    %clear previous AcceleratorObjects
waitbar(0.05,h);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BPM
% status field designates if BPM in use
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Define Horizontal BPMs
ifam = 'BPMx';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'BPM';
AO.(ifam).MemberOf                 = {'BPM'; 'HBPM'; 'PlotFamily'; 'Archivable'};
AO.(ifam).Monitor.Mode             = Mode;
%AO.(ifam).Monitor.Mode             = 'Special';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'm';
AO.(ifam).Monitor.PhysicsUnits     = 'm';
AO.(ifam).Monitor.SpecialFunctionGet = 'gethbpmaverage';
AO.(ifam).Simulated.NoiseActivated = 0; %To activate Noise on BPM reading in simulation   
%AO.(ifam).Monitor.SpecialFunctionGet = 'gethbpmaverage';
%AO.(ifam).Monitor.SpecialFunctionGet = 'gethturdevnumberyturn';

% ElemList devlist tangoname status common

varlist = writeBPMvarlist('H'); %Possible inputs are 'H' or 'V'

devnumber = length(varlist);

% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).Gain        = ones(devnumber,1);
AO.(ifam).Roll        = zeros(devnumber,1);
AO.(ifam).Golden      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Monitor.HW2PhysicsParams(:,:) = ones(devnumber,1);
AO.(ifam).Monitor.Physics2HWParams(:,:) = ones(devnumber,1);
AO.(ifam).Monitor.Handles(:,1)          = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType              = 'Scalar';

for k = 1: devnumber
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames{k}  = strcat(AO.(ifam).DeviceName{k}, '/XPosSA'); % Change
end

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2('BPM');
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = NaN;
end

AO.(ifam).History = AO.(ifam).Monitor;
AO.(ifam).History = rmfield(AO.(ifam).History,'SpecialFunctionGet');

dev = AO.(ifam).DeviceName;
AO.(ifam).History.TangoNames(:,:)       = strcat(dev, '/XPosSAHistory'); %Change
AO.(ifam).History.MemberOf = {'Plotfamily'};

AO.(ifam).Va = AO.(ifam).History;
AO.(ifam).Va.TangoNames(:,:)       = strcat(dev, '/VaSA'); %Change
AO.(ifam).Va.MemberOf = {'Plotfamily'};

AO.(ifam).Vb = AO.(ifam).History;
AO.(ifam).Vb.TangoNames(:,:)       = strcat(dev, '/VbSA'); %Change
AO.(ifam).Vb.MemberOf = {'Plotfamily'};

AO.(ifam).Vc = AO.(ifam).History;
AO.(ifam).Vc.TangoNames(:,:)       = strcat(dev, '/VcSA'); %Change
AO.(ifam).Vc.MemberOf = {'Plotfamily'};

AO.(ifam).Vd = AO.(ifam).History;
AO.(ifam).Vd.TangoNames(:,:)       = strcat(dev, '/VdSA'); %Change
AO.(ifam).Vd.MemberOf = {'Plotfamily'};

AO.(ifam).Sum = AO.(ifam).History;
AO.(ifam).Sum.TangoNames(:,:)       = strcat(dev, '/SumSA'); %Change
AO.(ifam).Sum.MemberOf = {'Plotfamily'};

AO.(ifam).Quad = AO.(ifam).History;
AO.(ifam).Quad.TangoNames(:,:)       = strcat(dev, '/QuadSA'); %Change
AO.(ifam).Quad.MemberOf = {'Plotfamily'};

AO.(ifam).Gaidevnumberpm = AO.(ifam).History;
AO.(ifam).Gaidevnumberpm.TangoNames(:,:)       = strcat(dev, '/Gain'); %Change
AO.(ifam).Gaidevnumberpm.MemberOf = {'Plotfamily'};

AO.(ifam).Switch = AO.(ifam).History;
AO.(ifam).Switch.TangoNames(:,:)       = strcat(dev, '/Switches'); %Change
AO.(ifam).Switch.MemberOf = {'Plotfamily'};

%% Define Vertical BPMs
ifam = 'BPMy';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'BPM';
AO.(ifam).MemberOf                 = {'BPM'; 'VBPM'; 'PlotFamily'; 'Archivable'};
AO.(ifam).Monitor.Mode             = Mode;
%AO.(ifam).Monitor.Mode             = 'Special';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'm';
AO.(ifam).Monitor.PhysicsUnits     = 'm';
AO.(ifam).Monitor.SpecialFunctionGet = 'getvbpmaverage';
AO.(ifam).Simulated.NoiseActivated = 0; %To activate Noise on BPM reading in simulation   
%AO.(ifam).Monitor.SpecialFunctionGet = 'gethbpmaverage';
%AO.(ifam).Monitor.SpecialFunctionGet = 'gethturdevnumberyturn';

% ElemList devlist tangoname status common

varlist = writeBPMvarlist('V'); %Possible inputs are 'H' or 'V'

devnumber = length(varlist);

% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).Gain        = ones(devnumber,1);
AO.(ifam).Roll        = zeros(devnumber,1);
AO.(ifam).Golden      = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Monitor.HW2PhysicsParams(:,:) = ones(devnumber,1);
AO.(ifam).Monitor.Physics2HWParams(:,:) = ones(devnumber,1);
AO.(ifam).Monitor.Handles(:,1)          = NaN*ones(devnumber,1);
AO.(ifam).Monitor.DataType              = 'Scalar';

for k = 1: devnumber
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames{k}  = strcat(AO.(ifam).DeviceName{k}, '/XPosSA'); % Change
end

% Group
if ControlRoomFlag
        AO.(ifam).GroupId = tango_group_create2('BPM');
        tango_group_add(AO.(ifam).GroupId,AO.(ifam).DeviceName');
else
    AO.(ifam).GroupId = NaN;
end

AO.(ifam).History = AO.(ifam).Monitor;
AO.(ifam).History = rmfield(AO.(ifam).History,'SpecialFunctionGet');

dev = AO.(ifam).DeviceName;
AO.(ifam).History.TangoNames(:,:)       = strcat(dev, '/XPosSAHistory'); %Change
AO.(ifam).History.MemberOf = {'Plotfamily'};

AO.(ifam).Va = AO.(ifam).History;
AO.(ifam).Va.TangoNames(:,:)       = strcat(dev, '/VaSA'); %Change
AO.(ifam).Va.MemberOf = {'Plotfamily'};

AO.(ifam).Vb = AO.(ifam).History;
AO.(ifam).Vb.TangoNames(:,:)       = strcat(dev, '/VbSA'); %Change
AO.(ifam).Vb.MemberOf = {'Plotfamily'};

AO.(ifam).Vc = AO.(ifam).History;
AO.(ifam).Vc.TangoNames(:,:)       = strcat(dev, '/VcSA'); %Change
AO.(ifam).Vc.MemberOf = {'Plotfamily'};

AO.(ifam).Vd = AO.(ifam).History;
AO.(ifam).Vd.TangoNames(:,:)       = strcat(dev, '/VdSA'); %Change
AO.(ifam).Vd.MemberOf = {'Plotfamily'};

AO.(ifam).Sum = AO.(ifam).History;
AO.(ifam).Sum.TangoNames(:,:)       = strcat(dev, '/SumSA'); %Change
AO.(ifam).Sum.MemberOf = {'Plotfamily'};

AO.(ifam).Quad = AO.(ifam).History;
AO.(ifam).Quad.TangoNames(:,:)       = strcat(dev, '/QuadSA'); %Change
AO.(ifam).Quad.MemberOf = {'Plotfamily'};

AO.(ifam).Gaidevnumberpm = AO.(ifam).History;
AO.(ifam).Gaidevnumberpm.TangoNames(:,:)       = strcat(dev, '/Gain'); %Change
AO.(ifam).Gaidevnumberpm.MemberOf = {'Plotfamily'};

AO.(ifam).Switch = AO.(ifam).History;
AO.(ifam).Switch.TangoNames(:,:)       = strcat(dev, '/Switches'); %Change
AO.(ifam).Switch.MemberOf = {'Plotfamily'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SLOW HORIZONTAL CORRECTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'HCOR';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'COR';
AO.(ifam).MemberOf                 = {'MachineConfig'; 'HCOR'; 'COR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode             = Mode;
AO.(ifam).Monitor.DataType         = 'Scalar';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'rad';
AO.(ifam).Monitor.PhysicsUnits     = 'rad';
AO.(ifam).Monitor.SpecialFunctionGet = 'gethst';

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'rad';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';


% elemlist devlist tangoname       status  common  attR           attW      range
varlist = writeHCORvarlist;

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).Gain        = ones(devnumber,1);
AO.(ifam).Roll        = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);

for k = 1: devnumber
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @corgetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
end

AO.(ifam).Setpoint.HW2PhysicsParams(:,:) = ones(devnumber,1);
AO.(ifam).Setpoint.Physics2HWParams(:,:) = ones(devnumber,1);

AO.(ifam).Setpoint.HW2PhysicsParams(:,:) = ones(devnumber,1);
AO.(ifam).Setpoint.Physics2HWParams(:,:) = ones(devnumber,1);

AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-2*ones(devnumber,1);
% Warning optics dependent cf. Low alpha lattice
AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(devnumber,1)*5e-6*2; % 2*5 urad (half used for kicking)

AO.(ifam).Monitor.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf     = {'PlotFamily'};
AO.(ifam).GroupId = nan;

AO.(ifam).Setpoint.SpecialFunctionSet = 'sethst';
AO.(ifam).Setpoint.SpecialFunctionGet = 'gethst';

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
% AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
%     AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SLOW VERTICAL CORRECTORS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ifam = 'VCOR';
AO.(ifam).FamilyName               = ifam;
AO.(ifam).FamilyType               = 'COR';
AO.(ifam).MemberOf                 = {'MachineConfig'; 'VCOR'; 'COR'; 'Magnet'; 'PlotFamily'; 'Archivable'};

AO.(ifam).Monitor.Mode             = Mode;
AO.(ifam).Monitor.DataType         = 'Scalar';
AO.(ifam).Monitor.Units            = 'Hardware';
AO.(ifam).Monitor.HWUnits          = 'rad';
AO.(ifam).Monitor.PhysicsUnits     = 'rad';
AO.(ifam).Monitor.SpecialFunctionGet = 'getvst';

AO.(ifam).Setpoint.Mode             = Mode;
AO.(ifam).Setpoint.DataType         = 'Scalar';
AO.(ifam).Setpoint.Units            = 'Hardware';
AO.(ifam).Setpoint.HWUnits          = 'rad';
AO.(ifam).Setpoint.PhysicsUnits     = 'rad';


% elemlist devlist tangoname       status  common  attR           attW      range
varlist = writeVCORvarlist;

devnumber = length(varlist);
% preallocation
AO.(ifam).ElementList = zeros(devnumber,1);
AO.(ifam).Status      = zeros(devnumber,1);
AO.(ifam).Gain        = ones(devnumber,1);
AO.(ifam).Roll        = zeros(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).DeviceName  = cell(devnumber,1);
AO.(ifam).CommonNames = cell(devnumber,1);
AO.(ifam).Monitor.TangoNames  = cell(devnumber,1);
AO.(ifam).Setpoint.TangoNames = cell(devnumber,1);
AO.(ifam).Setpoint.Range = zeros(devnumber,2);

for k = 1: devnumber
    AO.(ifam).ElementList(k)  = varlist{k,1};
    AO.(ifam).DeviceList(k,:) = varlist{k,2};
    AO.(ifam).DeviceName(k)   = deblank(varlist(k,3));
    AO.(ifam).Status(k)       = varlist{k,4};
    AO.(ifam).CommonNames(k)  = deblank(varlist(k,5));
    AO.(ifam).Monitor.TangoNames(k)  = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,6)));
    AO.(ifam).Setpoint.TangoNames(k) = strcat(AO.(ifam).DeviceName{k}, '/', deblank(varlist(k,7)));
    AO.(ifam).Setpoint.Range(k,:)      = varlist{k,8};
    % information for getrunflag
    AO.(ifam).Setpoint.RunFlagFcn = @corgetrunflag;
    AO.(ifam).Setpoint.RampRate = 1;
end

%Load fields from datablock
% AT use the "A-coefficients" for correctors plus an offset
setao(AO);

AO.(ifam).Setpoint.Tolerance(:,:)    = 1e-2*ones(devnumber,1);
% Warning optics dependent cf. Low alpha lattice
AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(devnumber,1)*5e-6*2; % 2*5 urad (half used for kicking)

AO.(ifam).Monitor.MemberOf      = {'PlotFamily'};
AO.(ifam).Setpoint.MemberOf     = {'PlotFamily'};
AO.(ifam).GroupId = nan;

AO.(ifam).Setpoint.SpecialFunctionSet = 'setvst';
AO.(ifam).Setpoint.SpecialFunctionGet = 'getvst';

%convert response matrix kicks to HWUnits (after AO is loaded to AppData)
setao(AO);   %required to make physics2hw function
% AO.(ifam).Setpoint.DeltaRespMat = physics2hw(AO.(ifam).FamilyName,'Setpoint', ...
%     AO.(ifam).Setpoint.DeltaRespMat, AO.(ifam).DeviceList);

%============
%% RF System
%============
ifam = 'RF';
AO.(ifam).FamilyName                = ifam;
AO.(ifam).FamilyType                = 'RF';
AO.(ifam).MemberOf                  = {'RF','RFSystem'};
AO.(ifam).Status                    = 1;
AO.(ifam).CommonNames               = 'RF';
AO.(ifam).DeviceList                = [1 1];
AO.(ifam).ElementList               = 1;
AO.(ifam).DeviceName(:,:)           = {'srrf/master-oscillator/1'};

%Frequency Readback
AO.(ifam).Monitor.Mode                = Mode;
% AO.(ifam).Monitor.Mode                = 'Special';
% AO.(ifam).Monitor.SpecialFunctionGet = 'getrf2';
AO.(ifam).Monitor.DataType            = 'Scalar';
AO.(ifam).Monitor.Units               = 'Hardware';
AO.(ifam).Monitor.HW2PhysicsParams    = 1;       %no hw2physics function necessary
AO.(ifam).Monitor.Physics2HWParams    = 1;
AO.(ifam).Monitor.HWUnits             = 'Hz';
AO.(ifam).Monitor.PhysicsUnits        = 'Hz';
AO.(ifam).Monitor.TangoNames          = {'srrf/master-oscillator/1/Frequency'};
AO.(ifam).Monitor.Handles             = NaN;
AO.(ifam).Monitor.Range               = [351e6 353e6];

AO.(ifam).Setpoint = AO.(ifam).Monitor;
AO.(ifam).Desired  = AO.(ifam).Monitor;

setao(AO);   %required to make physics2hw function


%======================================================================
%======================================================================
%% Append Accelerator Toolbox information
%======================================================================
%======================================================================
disp('** Initializing Accelerator Toolbox information');

AO = getao;

%% Machine Params
ifam = ('MachineParams');
AO.(ifam).AT.ATType       = 'MachineParams';
AO.(ifam).AT.ATName(1,:)  = 'Energy  ';
AO.(ifam).AT.ATName(2,:)  = 'current ';
AO.(ifam).AT.ATName(3,:)  = 'Lifetime';

% Save AO
setao(AO);

disp('Setting min max configuration from TANGO static database ...');

waitbar(0.80,h);

waitbar(0.95,h);

setoperationalmode(OperationalMode);

delete(h);

switch2sim;

end