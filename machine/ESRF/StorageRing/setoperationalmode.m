function setoperationalmode(ModeNumber)
%SETOPERATIONALMODE - Switches between the various operational modes
%  setoperationalmode(ModeNumber)
%
%  INPUTS
%  1. ModeNumber = number
%        1 '2.7391 GeV, 18.2020 10.3170', ...

%        100 'Laurent''s Mode'...
%
%  See also aoinit, updateatindex, esrfinit, setmmldirectories, lattice_prep
%  NOTES

%
% Written by Laurent S. Nadolski

global THERING

% Check if the AO exists
checkforao;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accelerator Dependent Modes %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 1
    ModeNumber = [];
end
if isempty(ModeNumber)
    ModeCell = {...
        ' 1/ 2.7391 GeV, 18.2020 10.3170', ...
        'Laurent''s Mode'...
        };
    [ModeNumber, OKFlag] = listdlg('Name','ESRF','PromptString','Select the Operational Mode:', ...
        'SelectionMode','single', 'ListString', ModeCell, 'ListSize', [450 200], 'InitialValue', 1);

    if OKFlag ~= 1
        fprintf('   Operational mode not changed\n');
        return
    elseif ModeNumber == length(ModeCell)
        ModeNumber = 100;  % Laurent
    end
end
%add ModeNumber and SpecialTag in AD structure
AD=getad;
AD.ModeNumber = ModeNumber;
%for specify mode Number in Prompt
AD.SpecialTag=''; 
setad(AD);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Accelerator Data Structure %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
AD = getad;
AD.Machine = 'ESRF';            % Will already be defined if setpathmml was used
AD.MachineType = 'StorageRing';   % Will already be defined if setpathmml was used
AD.SubMachine  = 'StorageRing';   % Will already be defined if setpathmml was used
AD.OperationalMode = '';          % Gets filled in later
AD.HarmonicNumber = 992;

% Defaults RF for dispersion and chromaticity measurements (must be in Hardware units)
AD.DeltaRFDisp = 60e-6;
AD.DeltaRFChro = [-100 -50 0 50 100] * 2*1e-6;

% Tune processor delay: delay required to wait
% to have a fresh tune measurement after changing
% a variable like the RF frequency.  Setpv will wait
% 2.2 * TuneDelay to be guaranteed a fresh data point.
%AD.BPMDelay  = 0.25; % use [N, BPMDelay]=getbpmaverages (AD.BPMDelay will disappear)
AD.TuneDelay = 1;

% The offset and golden orbits are stored at the end of this file
% TODO
%BuildOffsetAndGoldenOrbits;  % Local function
FileName_GoldenOrbit='GoldenBPM.mat'; %Default GoldenOrbit File you can specify another File in each operationalmode

% SP-AM Error level
% AD.ErrorWarningLevel = 0 -> SP-AM errors are Matlab errors {Default}
%                       -1 -> SP-AM errors are Matlab warnings
%                       -2 -> SP-AM errors prompt a dialog box
%                       -3 -> SP-AM errors are ignored (ErrorFlag=-1 is returned)
AD.ErrorWarningLevel = 0;

global CorrInSextup
CorrInSextup=0;
               
%%%%%%%%%%%%%%%%%%%%%
% Operational Modes %
%%%%%%%%%%%%%%%%%%%%%

% Mode setup variables (mostly path and file names)
% AD.OperationalMode - String used in titles
% ModeName - String used for mode directory name off DataRoot/MachineName
% OpsFileExtension - string add to default file names

 


%% ModeNumber == 1 ESRF-EBS
if ModeNumber == 1
    AD.OperationalMode = '6 GeV, 76.21 27.34';
    AD.Energy = 6.0;     % Make sure this is the same as bend2gev at the production lattice!
   
    ModeName = 'ESRF-EBS';
%     AD.ATModel='/mntdirect/_operation/beamdyn/matlab/optics/ebs/theory/betamodel.mat';
%     l=load(AD.ATModel);
%     THERING=l.betamodel;
    AD.ATModel='ESRF_EBS';
    run(AD.ATModel);
    OpsFileExtension = '_betamodel';
    
    %FileName_GoldenPHC='Conf_PHCs_WSV50_4.5mm_2019-01-18_10-03-11.mat';
    % Golden TUNE is with the TUNE family
    % 18.20 / 10.30
    AO = getao;
    AO.TUNE.Monitor.Golden = [
        0.21
        0.34
        NaN];
    AO.COUPLING.Golden = 1;


    % Golden chromaticity is in the AD (Physics units)
    AD.Chromaticity.Golden = [6.0 ; 4.0];

    setao(AO);
      
    % Response matrix kick in radians HCOR VCOR FHCOR FVCOR
    %local_setResponseMatrixKick(5e-6, 10e-6, 3e-6, 3e-6) 
        
    %local_set_config_mode('nanoscopiumconfig122B_6C'); % with 6 new correctors in October 2014
    %setfamilydata(1, 'BPMx', 'Status');
    %setfamilydata(1, 'BPMz', 'Status');
    %local_ChangeSextupolesLength({'S1','S2','S3','S4','S5','S6','S7','S8','S9','S10','S11','S12'}, 1e-8);  
    %local_SetMagnetCoeff('HCOR');
    %local_SetMagnetCoeff('VCOR');
    
    AO = getao;
 
%% ModeNumber == 100 Laurent
elseif ModeNumber == 100
    % User mode - Laurent
    AD.OperationalMode = '2.7391 GeV, 18.2 10.3';
    AD.Energy = 2.7391;     % Make sure this is the same as bend2gev at the production lattice!
    ModeName = 'chasmann_green';
    OpsFileExtension = '_chasmann_green';

    % AT lattice
    AD.ATModel = 'chasman_green';
    chasman_green;

    % Golden TUNE is with the TUNE family
    % 18.20 / 10.30
    AO = getao;
    AO.TUNE.Monitor.Golden = [
        0.20
        0.30
        NaN];


    % Golden chromaticity is in the AD (Physics units)
    AD.Chromaticity.Golden = [2; 2];

    setao(AO);
    %setfamilydata(ones(120,1)*1e-5, 'HCOR', 'Setpoint', 'DeltaRespMat');
    %setfamilydata(ones(120,1)*1e-5, 'VCOR', 'Setpoint', 'DeltaRespMat');
    % AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(nb,1)*0.5e-4*1; % 2*25 urad (half used for kicking)
    local_set_config_mode('normalconfig120');    
   % local_setmagnetcoefficient(@magnetcoefficients_new_calib_new_modele_low_alpha_janv2010);

 else
    error('Operational mode unknown');
end


% Force units to hardware
switch2hw;

% % Activation of correctors of HU640
% if ModeNumber == 6
%     switchHU640Cor('ON');
% else
%     switchHU640Cor('OFF');
% end

% Set the AD directory path
setad(AD);
MMLROOT = setmmldirectories(AD.Machine, AD.SubMachine, ModeName, OpsFileExtension);
AD = getad;

% SOLEIL specific path changes

% Top Level Directories

%AD.Directory.DataRoot       = fullfile(MMLROOT, 'measdata', 'SOLEIL', 'StorageRingdata', filesep);
% RUCHE
MMLDATAROOT = getmmldataroot;
AD.Directory.DataRoot       = fullfile(MMLDATAROOT, 'measdata', 'SOLEIL', 'StorageRingdata', filesep);
AD.Directory.Lattice        = fullfile(MMLROOT, 'machine', 'SOLEIL', 'StorageRing', 'Lattices', filesep);
AD.Directory.Orbit          = fullfile(MMLROOT, 'machine', 'SOLEIL', 'StorageRing',  'orbit', filesep);

% Data Archive Directories DO NOT REMOVE LINES
AD.Directory.BeamUser       = fullfile(AD.Directory.DataRoot, 'BPM', 'BeamUser', filesep); %store saved orbit for operation (every new beam)
AD.Directory.BPMData        = fullfile(AD.Directory.DataRoot, 'BPM', filesep);
AD.Directory.TuneData       = fullfile(AD.Directory.DataRoot, 'Tune', filesep);
AD.Directory.ChroData       = fullfile(AD.Directory.DataRoot, 'Chromaticity', filesep);
AD.Directory.DispData       = fullfile(AD.Directory.DataRoot, 'Dispersion', filesep);
AD.Directory.ConfigData     = fullfile(getmmlconfigroot, 'machine', 'SOLEIL', 'StorageRing', 'MachineConfig', filesep);
AD.Directory.BumpData       = fullfile(AD.Directory.DataRoot, 'Bumps', filesep);
AD.Directory.Archiving      = fullfile(AD.Directory.DataRoot, 'ArchivingData', filesep);
AD.Directory.QUAD           = fullfile(AD.Directory.DataRoot, 'QUAD', filesep);
AD.Directory.BBA            = fullfile(AD.Directory.DataRoot, 'BBA', filesep);
AD.Directory.BBAcurrent     = fullfile(AD.Directory.BBA, 'dafault' ,filesep);
AD.Directory.PINHOLE        = fullfile(AD.Directory.DataRoot, 'PINHOLE', filesep);
AD.Directory.Synchro        = fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'synchro', filesep);
AD.Directory.LOCOData       = fullfile(AD.Directory.DataRoot, 'LOCO', filesep);

AD.Directory.ConfigOpsData  = fullfile(getmmlconfigroot, 'machine', 'SOLEIL', 'StorageRingOpsData', filesep);
AD.Directory.BPMGolden      = fullfile(AD.Directory.ConfigOpsData, 'GoldenOrbit', filesep);
%AD.Directory.PHCGolden      = fullfile(AD.Directory.ConfigOpsData, 'GoldenPHC', filesep);
AD.Directory.LOCOGolden     = fullfile(AD.Directory.ConfigOpsData, 'GoldenLOCO', filesep);
AD.Directory.PSOData     = fullfile(AD.Directory.DataRoot, 'PSO', filesep);

% STANDALONE matlab applications
AD.Directory.Standalone     = fullfile(MMLROOT, '..', filesep, 'standalone_applications', filesep);

DATADIR = getdataroot;
% FOFB matlab applications
AD.Directory.FOFBdata     = fullfile(AD.Directory.DataRoot, 'FOFB', filesep);
AD.Directory.DG = fullfile(DATADIR,filesep,'DG', filesep, 'matlab', filesep);

% For coupling correction. Used by coupling.m
AD.Directory.Coupling     = fullfile(AD.Directory.DataRoot, 'SkewQuad', 'solution_QT');

% AD.Directory.InterlockData  = fullfile(AD.Directory.DataRoot, 'Interlock/'];

%Response Matrix Directories
AD.Directory.BPMResponse    = fullfile(AD.Directory.DataRoot, 'Response', 'BPM', filesep);
AD.Directory.TuneResponse   = fullfile(AD.Directory.DataRoot, 'Response', 'Tune', filesep);
AD.Directory.ChroResponse   = fullfile(AD.Directory.DataRoot, 'Response', 'Chrom', filesep);
AD.Directory.DispResponse   = fullfile(AD.Directory.DataRoot, 'Response', 'Disp', filesep);
AD.Directory.SkewResponse   = fullfile(AD.Directory.DataRoot, 'Response', 'Skew', filesep);

% used by energytunette
AD.Directory.BPMTransport   = fullfile(AD.Directory.DataRoot, 'Transport', 'BPM', filesep);
% used by MAT's Steerette application
AD.Directory.Steerette     = fullfile(AD.Directory.DataRoot, 'Transport', 'Steerette', filesep);

% Postmortem DATA
AD.Directory.BPMPostmortem    = fullfile(AD.Directory.DataRoot, 'Postmortem', 'BPMPostmortem', filesep);
AD.Directory.RFPostmortem     = fullfile(AD.Directory.DataRoot, 'Postmortem', 'RFPostmortem', filesep);

%Default Data File Prefix
AD.Default.BPMArchiveFile      = 'BPM';                %file in AD.Directory.BPM               orbit data
AD.Default.TuneArchiveFile     = 'Tune';               %file in AD.Directory.Tune              tune data
AD.Default.ChroArchiveFile     = 'Chro';               %file in AD.Directory.Chromaticity       chromaticity data
AD.Default.DispArchiveFile     = 'Disp';               %file in AD.Directory.Dispersion       dispersion data
AD.Default.CNFArchiveFile      = 'CNF';                %file in AD.Directory.CNF               configuration data
AD.Default.QUADArchiveFile     = 'QuadBeta';           %file in AD.Directory.QUAD             betafunction for quadrupoles
AD.Default.PINHOLEArchiveFile  = 'Pinhole';            %file in AD.Directory.PINHOLE             pinhole data
AD.Default.SkewArchiveFile     = 'SkewQuad';           %file in AD.Directory.SkewQuad             SkewQuad data
AD.Default.BBAArchiveFile      = 'BBA_DKmode';         %file in AD.Directory.BBA             BBA DK mode data

%Default Response Matrix File Prefix
AD.Default.BPMRespFile      = 'BPMRespMat';         %file in AD.Directory.BPMResponse       BPM response matrices
AD.Default.TuneRespFile     = 'TuneRespMat';        %file in AD.Directory.TuneResponse      tune response matrices
AD.Default.ChroRespFile     = 'ChroRespMat';        %file in AD.Directory.ChroResponse      chromaticity response matrices
AD.Default.DispRespFile     = 'DispRespMat';        %file in AD.Directory.DispResponse      dispersion response matrices
AD.Default.SkewRespFile     = 'SkewRespMat';        %file in AD.Directory.SkewResponse      skew quadrupole response matrices

%Orbit Control and Feedback Files
AD.Restore.GlobalFeedback   = 'Restore.m';

% Circumference
AD.Circumference = findspos(THERING,length(THERING)+1);
setad(AD);

% Updates the AT indices in the MiddleLayer with the present AT lattice
updateatindex;

% Set the model energy
setenergymodel(AD.Energy);


% Momentum compaction factor
MCF = getmcf('Model');
if isnan(MCF)
    AD.MCF = 8.5952e-05;
    fprintf('   Model alpha calculation failed, middlelayer alpha set to  %f\n', AD.MCF);
else
    AD.MCF = MCF;
    fprintf('   Middlelayer alpha set to %f (AT model).\n', AD.MCF);
end
setad(AD);


% Add Gain & Offsets for magnet family
fprintf('   Setting magnet monitor gains based on the production lattice.\n');
%setgainsandoffsets;


%%%%%%%%%%%%%%%%%%%%%%
% Final mode changes %
%%%%%%%%%%%%%%%%%%%%%%
%setlocodata('Nominal');

fprintf('   lattice files have changed or if the AT lattice has changed.\n');
fprintf('   Middlelayer setup for operational mode: %s\n', AD.OperationalMode);

setad(AD);

%% GOLDEN Orbit

%FileName_GoldenOrbit='GoldenBPM.mat';
% DirectoryName = getfamilydata('Directory','BPMGolden');
% FileName_GoldenOrbit=fullfile(DirectoryName,FileName_GoldenOrbit);
% FileStruct = load(FileName_GoldenOrbit); % setfamilydata(Golden(:,3),'BPMx','Golden',Golden(:,1:2));
% 
% setgolden(FileStruct.Data1.FamilyName ,FileStruct.Data1.Data, FileStruct.Data1.DeviceList);
% setgolden(FileStruct.Data2.FamilyName ,FileStruct.Data2.Data, FileStruct.Data2.DeviceList);
% %setgolden(FileName_GoldenOrbit); %Bug setgolden Ok but send an error.
% setfamilydata(FileStruct.Data1.TimeStamp,'BPMx','GoldenTimeStamp');%set a TimeStamp for GoldenOrbit
% setfamilydata(FileStruct.Data2.TimeStamp,'BPMy','GoldenTimeStamp');

% update getgolden in TANGO BPMmanager
EPSorbit = 1e-8;

%Golden=[FileStruct.Data1.DeviceList FileStruct.Data1.Data FileStruct.Data2.Data];

end

function local_InvertScaleFactor(family)
  AO = getao;
    if iscell(family)
        for i=1:length(family)
                %Inverted Powersupply you need to put ScaleFactor =-1 
                AO.(family{i}).Monitor.HW2PhysicsParams{2}(:)  =-1;
                AO.(family{i}).Monitor.Physics2HWParams{2}(:)  =-1;
        end 
        setao(AO);
    else
        error('Works only whith CellArray for Family')
    end    
end


function local_SetMagnetCoeff(ifam)
% Testing function for transition to have thick correctors
% Later on for general, change in magnet in soleilinit
% Warning with lowalpha

global CorrInSextup

AO = getao;

[Ccoefficients, Leff, MagnetType, Acoefficients] = magnetcoefficients(ifam);

len = length(family2status(ifam));

if CorrInSextup % Thick
    coefficients = Ccoefficients;
else % Thin
    coefficients =  Acoefficients;
end

for ii = 1:len,
    AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)   = coefficients;
    AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)   = coefficients;
    AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)  = coefficients;
    AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)  = coefficients;
end

AO.(ifam).Monitor.HW2PhysicsParams{1}(31,:)   = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Monitor.Physics2HWParams{1}(31,:)   = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Setpoint.HW2PhysicsParams{1}(31,:)  = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Setpoint.Physics2HWParams{1}(31,:)  = [0 0 0 0 0 0  2.475e-4 0]; % T.m
AO.(ifam).Monitor.HW2PhysicsParams{1}(32,:)   = [0 0 0 0 0 0 -2.443e-4 0]; % T.m
AO.(ifam).Monitor.Physics2HWParams{1}(32,:)   = [0 0 0 0 0 0 -2.443e-4 0]; % T.m
AO.(ifam).Setpoint.HW2PhysicsParams{1}(32,:)  = [0 0 0 0 0 0 -2.443e-4 0]; % T.m
AO.(ifam).Setpoint.Physics2HWParams{1}(32,:)  = [0 0 0 0 0 0 -2.443e-4 0]; % T.m


% % if CorrInSextup % Thick
% %     AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(len,1)*5e-6*2*-Leff; % 2*5 urad (half used for kicking)
% % else
% %     AO.(ifam).Setpoint.DeltaRespMat(:,:) = ones(len,1)*5e-6*2; % 2*5 urad (half used for kicking)
% % end    
% AO.(ifam).Setpoint.DeltaRespMat(31)  = 10e-6; %rad
% AO.(ifam).Setpoint.DeltaRespMat(32)  = 10e-6; %rad

setao(AO);
end

function local_ChangeSextupolesLength(family, Leff)
    global CorrInSextup
    
    if Leff > 1e-5
        CorrInSextup=1;
    else
        CorrInSextup=0;
    end
    
    AO = getao;
    if iscell(family)
        for i=1:length(family)
            %change the length of sextupoles 1e-8 (default) 0.16m (real value)
            AO.(family{i}).Monitor.HW2PhysicsParams{1}{2}  = Leff;
            AO.(family{i}).Monitor.Physics2HWParams{1}{2}  = Leff;
            AO.(family{i}).Setpoint.HW2PhysicsParams{1}{2} = Leff;
            AO.(family{i}).Setpoint.Physics2HWParams{1}{2} = Leff;
        end
         setao(AO);
    else
        error('Works only whith CellArray for Family')
    end    
end


function local_setmagnetcoefficient(magnetcoeff_function) %#ok<DEFNU>
% quadrupole magnet coefficients
% number of status 1 quadrupole families

AO = getao;
    
quadFamList = {'Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', ...
         'Q7', 'Q8', 'Q9', 'Q10'};
    
if family2status('Q11',1)
        quadFamList = [quadFamList, {'Q11'}];
end
    
if family2status('Q12',1)
        quadFamList = [quadFamList, {'Q12'}];
end
        
    
for k = 1:length(quadFamList)
        ifam = quadFamList{k};

        HW2PhysicsParams = feval(magnetcoeff_function, AO.(ifam).FamilyName);
        Physics2HWParams = HW2PhysicsParams;

        nb = size(AO.(ifam).DeviceName,1);

        for ii=1:nb
            val = 1.0;
            AO.(ifam).Monitor.HW2PhysicsParams{1}(ii,:)                 = HW2PhysicsParams;
            AO.(ifam).Monitor.HW2PhysicsParams{2}(ii,:)                 = val;
            AO.(ifam).Monitor.Physics2HWParams{1}(ii,:)                 = Physics2HWParams;
            AO.(ifam).Monitor.Physics2HWParams{2}(ii,:)                 = val;
            AO.(ifam).Setpoint.HW2PhysicsParams{1}(ii,:)                = HW2PhysicsParams;
            AO.(ifam).Setpoint.HW2PhysicsParams{2}(ii,:)                = val;
            AO.(ifam).Setpoint.Physics2HWParams{1}(ii,:)                = Physics2HWParams;
            AO.(ifam).Setpoint.Physics2HWParams{2}(ii,:)                = val;
        end
end

% sextupole magnet coefficients
% number of status 1 sextupole families
sextuFamList = {'S1', 'S2', 'S3', 'S4', 'S5', 'S6', ...
         'S7', 'S8', 'S9', 'S10'};
    
if family2status('S11',1)
        sextuFamList = [sextuFamList, {'S11'}];
end
    
if family2status('S12',1)
        sextuFamList = [sextuFamList, {'S12'}];
end

for k = 1:length(sextuFamList)
        ifam = sextuFamList{k};
        
        HW2PhysicsParams = feval(magnetcoeff_function, AO.(ifam).FamilyName);
        Physics2HWParams = HW2PhysicsParams;
        
        val = 1.0;
        AO.(ifam).Monitor.HW2PhysicsParams{1}(1,:)                 = HW2PhysicsParams;
        AO.(ifam).Monitor.HW2PhysicsParams{2}(1,:)                 = val;
        AO.(ifam).Monitor.Physics2HWParams{1}(1,:)                 = Physics2HWParams;
        AO.(ifam).Monitor.Physics2HWParams{2}(1,:)                 = val;
        AO.(ifam).Setpoint.HW2PhysicsParams{1}(1,:)                 = HW2PhysicsParams;
        AO.(ifam).Setpoint.HW2PhysicsParams{2}(1,:)                 = val;
        AO.(ifam).Setpoint.Physics2HWParams{1}(1,:)                 = Physics2HWParams;
        AO.(ifam).Setpoint.Physics2HWParams{2}(1,:)                 = val;
end
setao(AO);

end

function  local_setResponseMatrixKick(hcorK, vcorK, fhcorK, fvcorK)
AO = getao;
% local_setResponseMatrixKick - set absolute kick for responsematrix in
% urad
    fac = physics2hw(AO.HCOR.FamilyName,'Setpoint', 1 , AO.HCOR.DeviceList);    
    setfamilydata(ones(length(AO.HCOR.ElementList),1).*fac*hcorK*2, 'HCOR', 'Setpoint', 'DeltaRespMat');
    fac = physics2hw(AO.VCOR.FamilyName,'Setpoint', 1 , AO.VCOR.DeviceList);    
    setfamilydata(ones(length(AO.VCOR.ElementList),1).*fac*vcorK*2, 'VCOR', 'Setpoint', 'DeltaRespMat');
     
    fac = physics2hw(AO.FHCOR.FamilyName,'Setpoint', 1 , AO.FHCOR.DeviceList);    
    setfamilydata(ones(length(AO.FHCOR.ElementList),1).*fac*fhcorK*2, 'FHCOR', 'Setpoint', 'DeltaRespMat');
    fac = physics2hw(AO.FVCOR.FamilyName,'Setpoint', 1 , AO.FVCOR.DeviceList);    
    setfamilydata(ones(length(AO.FVCOR.ElementList),1).*fac*fvcorK*2, 'FVCOR', 'Setpoint', 'DeltaRespMat');
end

function  local_set_config_mode(configmode)
% Function for activating new families of quadrupole and sextupoles
% magnets.

switch(configmode)
    case 'S11config120' % with S11 120 BPMs to be obsolete
        setfamilydata(1, 'S11', 'Status')
        setfamilydata(0, 'S12', 'Status')
        setfamilydata(0, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(0, 'Q12', 'Status')
        setfamilydata(0, 'HCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'VCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'CycleHCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'CycleVCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(0, 'BPMz', 'Status', [13 8; 13 9]);
    case 'S11config122' % with S11 122 BPMs
        setfamilydata(1, 'S11', 'Status')
        setfamilydata(0, 'S12', 'Status')
        setfamilydata(0, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(0, 'Q12', 'Status')
        setfamilydata(1, 'HCOR', 'Status', [13 8]);
        setfamilydata(1, 'VCOR', 'Status', [13 9]);
        setfamilydata(1, 'CycleHCOR', 'Status', [13 8]);
        setfamilydata(1, 'CycleVCOR', 'Status', [13 9]);
        setfamilydata(0, 'HCOR', 'Status', [12 6; 13 9; 13 2]);
        setfamilydata(0, 'VCOR', 'Status', [12 7; 13 8; 13 1]);
        setfamilydata(0, 'CycleHCOR', 'Status', [12 6; 13 9; 13 2]);
        setfamilydata(0, 'CycleVCOR', 'Status', [12 7; 13 8; 13 1]);
        setfamilydata(1, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(1, 'BPMz', 'Status', [13 8; 13 9]);
    case 'normalconfig' % without S11 120 BPMs to be obsolete
        setfamilydata(0, 'S11', 'Status')
        setfamilydata(0, 'S12', 'Status')
        setfamilydata(0, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(0, 'Q12', 'Status')
        setfamilydata(0, 'HCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'VCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'CycleHCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'CycleVCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(0, 'BPMz', 'Status', [13 8; 13 9]);
    case 'nanoscopiumconfig120' % 120 BPMs to be obsolete
        setfamilydata(1, 'S11', 'Status')
        setfamilydata(1, 'S12', 'Status')
        setfamilydata(1, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(1, 'Q12', 'Status')
        setfamilydata(0, 'HCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'VCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'CycleHCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'CycleVCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(0, 'BPMz', 'Status', [13 8; 13 9]);
    case 'nanoscopiumconfig122' % 122 BPMs 
        setfamilydata(1, 'S11', 'Status')
        setfamilydata(1, 'S12', 'Status')
        setfamilydata(1, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(1, 'Q12', 'Status')
        setfamilydata(0, 'HCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'VCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(0, 'CycleHCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(0, 'CycleVCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(1, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(1, 'BPMz', 'Status', [13 8; 13 9]);
    case 'nanoscopiumconfig122C'
        setfamilydata(1, 'S11', 'Status')
        setfamilydata(1, 'S12', 'Status')
        setfamilydata(1, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(1, 'Q12', 'Status')
        setfamilydata(1, 'HCOR', 'Status', [13 8]);
        setfamilydata(1, 'VCOR', 'Status', [13 9]);
        setfamilydata(1, 'CycleHCOR', 'Status', [13 8]);
        setfamilydata(1, 'CycleVCOR', 'Status', [13 9]);
        setfamilydata(0, 'HCOR', 'Status', [12 6; 13 9; 13 2]);
        setfamilydata(0, 'VCOR', 'Status', [12 7; 13 8; 13 1]);
        setfamilydata(0, 'CycleHCOR', 'Status', [12 6; 13 9; 13 2]);
        setfamilydata(0, 'CycleVCOR', 'Status', [12 7; 13 8; 13 1]);
        setfamilydata(1, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(1, 'BPMz', 'Status', [13 8; 13 9]);
     case 'nanoscopiumconfig122B_6C'
        setfamilydata(1, 'S11', 'Status')
        setfamilydata(1, 'S12', 'Status')
        setfamilydata(1, 'Q11', 'Status', [13 1; 13 2])
        setfamilydata(1, 'Q12', 'Status')
        setfamilydata(1, 'HCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(1, 'VCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(1, 'CycleHCOR', 'Status', [13 8; 12 6; 13 9; 13 2]);
        setfamilydata(1, 'CycleVCOR', 'Status', [13 9; 12 7; 13 8; 13 1]);
        setfamilydata(1, 'BPMx', 'Status', [13 8; 13 9]);
        setfamilydata(1, 'BPMz', 'Status', [13 8; 13 9]);   
    otherwise
        error('Wrong mode')
end

% switch addition corrector for HU640... TO BE REMOVED LATER
switchHU640Cor('OFF');

end

