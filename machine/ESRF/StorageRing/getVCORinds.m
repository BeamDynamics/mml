function [vcorInds] = getVCORinds()

    global THERING

    vcorInds=findcells(THERING, 'FamName', 'S[HFDIJ]\w*');
    
    vcorInds = vcorInds(:);
end