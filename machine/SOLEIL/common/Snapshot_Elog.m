function Snapshot_Elog(hObject,callbackdata,handles,schedule,simplefig)
% Make Snapshot_of current figure and put png file in ELOG Folder

if schedule
    fig=hObject;
    
elseif simplefig
    fig=hObject;
    
else
    fig=handles.figure1;
end    

namefig=get(fig,'Name'); % get figure name
namefig=namefig(find(~isspace(namefig)));%remove space char
namefig(regexp(namefig,'[,,:,(,)]'))=[];
ElogPath='/home/data/FC/Elog/Photo Elog'; %define where to save snapshot

FormatOut='dd-mm-yy'; %specify format date of folder
FormatOut2='HHhMM_'; %specify format date of file

DateDay=datestr(now,FormatOut); 
w=weeknum(datenum(datestr(now,'dd-mmm-yyyy')));
DirToSnap=fullfile(ElogPath,['Semaine ' num2str(w,'%.2d')],DateDay);
[s,mess,messid]=mkdir(DirToSnap);
FileToSnap=fullfile(DirToSnap,[datestr(now,FormatOut2) namefig '.png' ]);

set(fig,'InvertHardCopy','off');%keepbackgroundColor
set(fig,'PaperPositionMode','auto');%set goodsize and reolution for saveas
set(fig,'PaperUnits','centimeters');
set(fig,'PaperPosition',[0 0 30 20]);
%print('ScreenSizeFigure','-dpng','-r0');
if schedule
    dtevec=clock;
    if (dtevec(4)==7 | dtevec(4)==15 |dtevec(4)==23)& dtevec(5)==0
            if ~exist(FileToSnap)
                saveas(fig,FileToSnap);
            end
    end
else
    saveas(fig,FileToSnap);
end


end