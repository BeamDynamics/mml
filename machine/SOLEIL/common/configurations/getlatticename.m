function latticeName = getlatticename
% GETLATTICENAME - give the lattice name currently in used in AT

%% Written by Laurent S. Nadolski

latticeName = getfamilydata('ATModel');