# set path for standalone matlab_application
#export
#LD_LIBRARY_PATH=/usr/Local/matlab/runtime/glnx86:/usr/Local/matlab/bin/glnx86:/usr/Local/matlab/sys/os/glnx86:/usr/Local/matlab/sys/java/jre/glnx86/jre1.5.0/lib/i386/native_threads:/usr/Local/matlab/sys/java/jre/glnx86/jre1.5.0/lib/i386/server:/usr/Local/matlab/sys/java/jre/glnx86/jre1.5.0/lib/i386/client:/usr/Local/matlab/sys/java/jre/glnx86/jre1.5.0/lib/i386:/usr/Local/gcc-3.4.6/lib:/usr/Local/soleil-root/bindings/matlab/2007a/mex-file:$LD_LIBRARY_PATH
# Modification for Matlab2009
#BINDING=/usr/Local/soleil-root/bindings/matlab/2007a/mex-file
BINDING=/usr/Local/soleil-root/bindings/matlab/2009b/mex-file
export
LD_LIBRARY_PATH=/usr/Local/matlab/runtime/glnx86:/usr/Local/matlab/bin/glnx86:/usr/Local/matlab/sys/os/glnx86:/usr/Local/matlab/sys/java/jre/glnx86/jre/lib/i386/native_threads:/usr/Local/matlab/sys/java/jre/glnx86/jre/lib/i386/server:/usr/Local/matlab/sys/java/jre/glnx86/jre/lib/i386/client:/usr/Local/matlab/sys/java/jre/glnx86/jre1.5.0/lib/i386:/usr/Local/gcc-4.4.2/lib:$BINDING:$LD_LIBRARY_PATH
