function topup_thebeam(targetValue)

currentPrecision = 0.1; % mA

dcctValue = getdcct;

deviceTopup  = 'BOO/AE/PSTopUpModeManager';
deviceRampRF = 'BOO/RF/RAMPETENSION.2';
%deviceRampRF = 'BOO/RF/RAMPETENSION';

%wake-up Booster
tango_command_inout2(deviceTopup, 'Nominal');
tango_command_inout2(deviceRampRF, 'Start');
pause(5)

while (targetValue-dcctValue) > currentPrecision
    fprintf('DCCT value %3.2f mA (target:%3.2f mA) \n', dcctValue, targetValue) ;
    burst_trigger
    pause(2)
    dcctValue = getdcct;
end

%Put Booster in eco mode and stop RF ramp of the Boost
tango_command_inout2(deviceTopup, 'Eco');
tango_command_inout2(deviceRampRF, 'Stop');

disp('Top-up finished')