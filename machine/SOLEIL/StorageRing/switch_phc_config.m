function res = switch_phc_config(varargin)
%
%
%  Optional input arguments
%    'DisplayConfigFile    - Get PHC parameters a file and display it
%    'Get'                 - Get PHC param from devices and save in file.
%    'Set'                 - Get PHC param from file and set in devices.
%    'NoGet'               - Get PHC param from devices and save in file.
%    'NoSet'               - Get PHC param from file and set in device.
%    'SetFile' + FilePath  - Get PHC param from File and set in devices.
%    'Compare' + FilePath  - Compare actual setup of devices and value in file.
%    'PHC_List' + value    - if you want only set some PHC ex:  switch_phc_config('Set','PHC_List',{'ANS-C02/DG/PHC-EMIT';'ANS-C16/DG/PHC-EMIT'})
%    'Attr_List' + value   - if you want only set some attribute of PHC ex:  switch_phc_config('Set','Attr_List',attr_list = {'AlphaX','AlphaZ'})
%
%  EXAMPLES
%  1. standard 
%      switch_phc_config;
%  2. to set PHC devices to Golden Values
%      switch_phc_config('SetFile','/home/data/matlab/data4mml/measdata/SOLEIL/StorageRingdata/PINHOLE/PHC_conf_standard.mat');
%  3. Display a file contents selected by the user 
%     switch_phc_config('DisplayConfigFile')
%  

%
%  Written by A.Bence, L. Nadolski
%%

%% to rename switch_phc_config

varargin2     = {};
res=1;
PHCread=[];

% put Flag to default setup
DisplayFLAG = 1;
DisplayConfigFileFLAG = 0;
SetFLAG = 1;
GetFLAG = 1;
SetFileFLAG = 0;
CompareFlag = 0;

% default list of devices and attributes
%dev_list = {'ANS-C02/DG/PHC-EMIT';'ANS-C04/DG/PHC-EMIT';'ANS-C16/DG/PHC-EMIT'};
dev_list = {'ANS-C02/DG/PHC-EMIT';'ANS-C16/DG/PHC-EMIT'};
attr_list = {'AlphaX','AlphaZ','BetaX','BetaZ','EtaX','EtaZ','EtaPX','EtaPZ'};

%flatten cellarray if you give all argins in one variable
for i = length(varargin):-1:1    
    if iscell(varargin{i})
        varargin=[varargin{:}];
    end    
end    

for i = length(varargin):-1:1    
    if strcmpi(varargin{i},'Get')
        GetFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Set')
        SetFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Display')
        DisplayFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'DisplayConfigFile')
        DisplayConfigFileFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'NoGet')
        GetFLAG = 0;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'NoSet')
        SetFLAG = 0;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'SetFile')
        SetFileFLAG = 1;
        SetFLAG = 0;
        DisplayFLAG = 0;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
        FullFilePath=varargin(i);
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Compare')
        DisplayFLAG = 0;
        SetFLAG = 0;
        GetFLAG = 0;
        CompareFlag=1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
        FullFilePath=varargin(i);
        varargin(i) = [];
    elseif strcmpi(varargin{i},'PHC_List')
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
        dev_list=varargin(i);
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Attr_List')
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
        attr_list=varargin(i);
        varargin(i) = [];
    end    
end 

%%
if DisplayConfigFileFLAG
    [fileName, pathName]=uigetfile(fullfile(getfamilydata('Directory','PINHOLE'),'conf_phc'),'Reading file');
    
    if fileName ~= 0
        load(fullfile(pathName,fileName));
        fprintf('fileName: %s\n', fileName);
        for ik =1:length(attr_list),
            fprintf('%10s PHC1 =%+8.04f PHC3 = %+8.04f\n', attr_list{ik}, ...
                PHC(1).(attr_list{ik}).value(1), PHC(2).(attr_list{ik}).value(1));
        end
    else
        fprintf('No file selected. Action aborded\n');
    end
    res = 0;
    return;
end

if GetFLAG ||  CompareFlag
    for j=1:length(dev_list)   
        devemit = dev_list{j};
        %disp(strcat('Read PHC device: ',devemit));
        %clean_name=strrep(strrep(devemit,'/','_'),'-','_');
        PHC(j).name=devemit;
        for i=1:length(attr_list)
            temp = tango_read_attribute2(devemit,attr_list{i});
            PHC(j).(attr_list{i})=temp;
        end   
    end
    if CompareFlag
        PHCread=PHC;
    end    
    if DisplayFLAG         
        disp('Before modification')       
        for ik =1:length(attr_list),
            fprintf('%10s PHC1 =%+8.04f PHC3 = %+8.04f\n', attr_list{ik}, ...
                PHC(1).(attr_list{ik}).value(1), PHC(2).(attr_list{ik}).value(1));
        end        
    end

    if GetFLAG
         fileName=strcat(appendtimestamp('Conf_PHCs'),'.mat');
         PathPHC=getfamilydata('Directory','PINHOLE');
         fullpath=fullfile(PathPHC,'conf_phc',fileName);
         [fileName, pathName]=uiputfile(fullpath,'Sauvegarde de la conf PHC 1 & 3 actuel sinon faite Cancel');    
    else
         fileName=0;
         pathName=0;
    end    
    if isequal(fileName,0) || isequal(pathName,0)
          %disp('User pressed cancel: no configuration saved')       
    else
        save(fullfile(pathName,fileName),'PHC');
    end

end    

 
 
 
 if SetFLAG
    [fileName, pathName]=uigetfile(fullfile(getfamilydata('Directory','PINHOLE'),'conf_phc'),'Chargement de la conf PHC 1 & 3 dans les devices');
 elseif SetFileFLAG  || CompareFlag   
     [pathName,fileName,EXT,VERSN]=fileparts(FullFilePath{:});
     fileName=[fileName,EXT];
 else 
     fileName=0;
     pathName=0;
 end   
 if isequal(fileName,0) || isequal(pathName,0)
        %disp('User pressed cancel: no configuration loaded')       
 else
        load(fullfile(pathName,fileName));
        if CompareFlag 
            for k=1:length(PHC)
                field=fieldnames(PHC(k));
                for l=1:length(field)
                    if strcmp(field{l},'name')
                        if PHC(k).(field{l})~=PHCread(k).(field{l})
                           res=0;
                           break; 
                        end    
                    else    
                        if PHC(k).(field{l}).value~=PHCread(k).(field{l}).value
                            res=0;
                            break;
                        end    
                    end    
                end    
            end    
            
        else    
            for j=1:length(dev_list)
                devemit = dev_list{j};
                disp(strcat('modification de :',devemit));
                for i=1:length(attr_list)
                    tango_write_attribute2(devemit,attr_list{i},PHC(j).(attr_list{i}).value(1));    
                end
            end
            disp('PHC modifié'); 
            if GetFLAG
                fprintf('After modification (file %s)\n', fileName)
                for ik =1:length(attr_list),
                    fprintf('%10s PHC1 =%+8.04f PHC3 = %+8.04f\n', attr_list{ik}, ...
                    PHC(1).(attr_list{ik}).value(1), PHC(2).(attr_list{ik}).value(1));
                end
            end
        end    
 end  

 
