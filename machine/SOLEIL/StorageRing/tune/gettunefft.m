function varargout = gettunefft
%  gettunefft - Get the betatron tune by FFT using the shaker
%
%  OUPUTS
%  1. tune - tune obtained by FFT
%
%  NOTES
%  FBT dta : tango_read_attribute2('ANS/RF/BBFV3-1', 'selectedBunchDataADC0')

%
%% Written by Laurent S. Nadolski  

DisplayFlag = 1;

if ~readattribute('ANS-C14/DG/NOE.H/State') || readattribute('ANS-C14/DG/NOE.V/State') 
    warning('Shaker not ON'); 
    % Commented since one needs also to check the amplitude
    %tango_command_inout2('ANS-C14/DG/NOE.H', 'On')
    %tango_command_inout2('ANS-C14/DG/NOE.V', 'On')
end
    
    
XFFT = tango_read_attribute2('ANS/DG/BPM-TUNEX', 'FFTord');
Xabs = tango_read_attribute2('ANS/DG/BPM-TUNEX', 'FFTabs');

YFFT = tango_read_attribute2('ANS/DG/BPM-TUNEZ', 'FFTord');
Yabs = tango_read_attribute2('ANS/DG/BPM-TUNEZ', 'FFTabs');

figure(50)
clf;
istart=1;

subplot(2,1,1);
semilogy(Xabs.value(istart:end), XFFT.value(istart:end));
xlim([0.1 0.3])
ylim([1e-15 1e-8])

subplot(2,1,2);
semilogy(Yabs.value(istart:end), YFFT.value(istart:end));
xlim([0.1 0.3])
ylim([1e-15 1e-8])

[Ymax Yval] = max(YFFT.value(100:end-100));
Tune(2) = Yabs.value(Yval+100);

[Xmax Xval] = max(XFFT.value(100:end-100));
Tune(1) = Xabs.value(Xval+100);

%% Display to the screen
if DisplayFlag
   fprintf('\n  Horizontal Tune = %6.4f\n', Tune(1));
   fprintf('    Vertical Tune = %6.4f\n\n', Tune(2));
end

if nargout >0
    varargout{1} = Tune;
end
%steptune(getgolden('TUNE', [1 1; 1 2])-gettune_fft)