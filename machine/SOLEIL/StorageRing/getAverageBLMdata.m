function data_struct = getAverageBLMdata(BLMgroup, nSeconds)
% getAverageBLMdata - Get beam loss monitor averaged data
%  
%  INPUTS
%  1. BLMgroup - TANGO group for beam loss monitors
%  2. nSecond  - Average data of the last nSecond (14 Hz data acquisition)

% data @ 14 Hz
%nSeconds = 60;
nData = 14*nSeconds;

data_attr_list={'SaHistoryA','SaHistoryB','SaHistoryC','SaHistoryD'};
maxADC_attr_list={'MaxAdcA', 'MaxAdcB', 'MaxAdcC', 'MaxAdcD'};
%termination_attr_list={'TerminationA', 'TerminationB', 'TerminationC', 'TerminationD'};
%attenuation_attr_list={ 'AttenuationA','AttenuationB','AttenuationC','AttenuationD'};
%Vgc_attr_list={ 'BldVgcOutputA','BldVgcOutputB','BldVgcOutputC','BldVgcOutputD'};


data_result=tango_group_read_attributes2(BLMgroup,data_attr_list,0);
maxADC_result=tango_group_read_attributes2(BLMgroup,maxADC_attr_list,0);
%termination_result=tango_group_read_attributes2(BLMgroup,termination_attr_list,0);
%attenuation_result=tango_group_read_attributes2(BLMgroup,attenuation_attr_list,0);
%Vgc_result=tango_group_read_attributes2(BLMgroup,Vgc_attr_list,0);

Ndev=size(data_result.dev_replies,2);
Nattr=size(data_attr_list,2);

for i=1:Ndev
    for j=1:Nattr
        data_struct.devicelist{4*(i-1)+j,1}=data_result.dev_replies(i).attr_values(j).dev_name;
        data_struct.attrlist{4*(i-1)+j,1}=data_result.dev_replies(i).attr_values(j).attr_name;
        data_struct.data(4*(i-1)+j,:)=data_result.dev_replies(i).attr_values(j).value;
        data_struct.maxADC(4*(i-1)+j,1)=maxADC_result.dev_replies(i).attr_values(j).value(1);
        %data_struct.termination(4*(i-1)+j,1)=termination_result.dev_replies(i).attr_values(j).value(1);
        %data_struct.attenuation(4*(i-1)+j,1)=attenuation_result.dev_replies(i).attr_values(j).value(1);
        %data_struct.Vgc(4*(i-1)+j,1)=Vgc_result.dev_replies(i).attr_values(j).value(1);
    end
end
data_struct.time = data_result.dev_replies(1).attr_values(1).time;

%averagedValue = mean(tmp.value(end-nData:end));
data_struct.averagedValue = mean(data_struct.data(:,end-nData:end),2);
