function Struct = getHalo(SpecificName)

%% Choose here parameters
% toto
SpecificName            = 'lowalpha_QTmin_wHalo_woID_389_304_PHC3_m50Hz_wOrbitCorr_nuzCorr';  %'HU44_TEMPO' ;
ArchiveProfilesFlag     = 1;                    % 1=Archive profiles
Struct.devName          = 'ANS-C16/DG/' ;
Struct.lifetime         = 0 ; %readattribute('ANS/DG/DCCT-CTRL/lifeTime');
Struct.fs               = 0 ;
Struct.fs_2             = 0 ;

%% Initialization

DirectoryName   = [ getfamilydata('Directory', 'PINHOLE') 'HALO/']
FileName2       = [DirectoryName, prependtimestamp('HALO_'), SpecificName]

% Starting time
t0 = clock;

Struct.height_max      = 494 ; % nb pixels max camera
Struct.width_max       = 659 ; % nb pixels

%% Define devices

devVG           = [Struct.devName 'PHC-VG'];
devAna          = [Struct.devName 'PHC-IMAGEANALYZER'];
devemit         = [Struct.devName 'PHC-EMIT'];

%% Get and save profiles / image

pause(2);
prov = tango_read_attribute(devAna, 'XProj');
Struct.ProfileX          = prov.value; 
prov = tango_read_attribute(devAna, 'YProj');
Struct.ProfileY          = prov.value; 
prov = tango_read_attribute(devAna, 'XProjFitted');
Struct.ProfileXFitted    = prov.value; 
prov = tango_read_attribute(devAna, 'YProjFitted');
Struct.ProfileYFitted          = prov.value; 

figure(500) ; hold all ; plot(Struct.ProfileX,'linewidth',3) ;% hold all; plot(Struct.ProfileXFitted,'k--') 
set(gca,'Fontsize',14) ; title('Horizontal projection')
xlabel('# pixel') ; ylabel('Intensity') ; xlim([0  Struct.height_max])
figure(600) ; hold all ; plot(Struct.ProfileY,'linewidth',3) ; %hold all ; plot(Struct.ProfileYFitted,'k--') 
set(gca,'Fontsize',14) ; title('Vertical projection')
xlabel('# pixel') ; ylabel('Intensity') ; xlim([0  Struct.width_max])

Struct.SigmaX = readattribute([devAna '/XProjFitSigma']);
Struct.SigmaY = readattribute([devAna '/YProjFitSigma']);

prov = tango_read_attribute2(devVG,'image');
Struct.image            = prov.value' ;
figure(700) ; imagesc(Struct.image)
caxis([0 200])

%% save other parameters
Struct.emittanceX       = readattribute([devemit '/EmittanceH']);
Struct.emittanceZ       = readattribute([devemit '/EmittanceV']);
Struct.ExposureTime       = readattribute([devVG '/exposureTime']);

Struct.current          = getdcct ;
Nu = gettune;
Struct.Nux              = Nu(1) ;
Struct.Nuz              = Nu(2) ;
Struct.TimeStamp        = datestr(clock);

Struct.gapCRISTAL       = readattribute('ANS-C06/EI/C-U20/gap');
Struct.gapTEMPO         = readattribute('ANS-C08/EI/M-HU44.1/gap');
Struct.gapSEXTANTS_1     = readattribute('ANS-C14/EI/M-HU44.1/gap');
Struct.gapSEXTANTS_2     = readattribute('ANS-C14/EI/M-HU80.2/gap');

Struct.scraperV_bas     = readattribute('ANS-C01/DG/SCRA_V-MORS.BAS/position');
Struct.scraperV_haut    = readattribute('ANS-C01/DG/SCRA_V-MORS.HAUT/position');

Struct.vide             = readattribute('ANS/VI/CALC-PI.1/mean');

Struct.frf              = readattribute('ANS/RF/MasterClock/frequency');
Struct.Vrf              = readattribute('ANS/RF/TANGOPARSER/TotalVoltageRF');

% save
if ArchiveProfilesFlag
    save(FileName2, 'Struct');
end

disp('OK')
