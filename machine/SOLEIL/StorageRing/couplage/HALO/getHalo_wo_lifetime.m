function Struct = getHalo_wo_lifetime(SpecificName)

%% Choose here parameters

%SpecificName            = 'Operation_Hybride';  %'HU44_TEMPO' ;
ArchiveProfilesFlag     = 1;                    % 1=Archive profiles
Struct.devName                 = 'ANS-C16/DG/' ;

%% Initialization

DirectoryName   = [ getfamilydata('Directory', 'PINHOLE') 'HALO/']
FileName2       = [DirectoryName, prependtimestamp('HALO_data_'), SpecificName]

% Starting time
t0 = clock;


%% Define devices

devVG           = [Struct.devName 'PHC-VG'];
devAna          = [Struct.devName 'PHC-IMAGEANALYZER'];
devemit         = [Struct.devName 'PHC-EMIT'];

%% Get and save profiles / image

pause(2);
prov = tango_read_attribute(devAna, 'XProj');
Struct.ProfileX          = prov.value; 
prov = tango_read_attribute(devAna, 'YProj');
Struct.ProfileY          = prov.value; 
prov = tango_read_attribute(devAna, 'XProjFitted');
Struct.ProfileXFitted    = prov.value; 
prov = tango_read_attribute(devAna, 'YProjFitted');
Struct.ProfileYFitted          = prov.value; 

figure(50) ; hold all ; plot(Struct.ProfileX,'linewidth',3) ; hold on ; plot(Struct.ProfileXFitted,'k--') 
set(gca,'Fontsize',14) ; title('Horizontal projection')
xlabel('# pixel') ; ylabel('Intensity') ; xlim([0  Struct.height_max])
figure(60) ; hold all ; plot(Struct.ProfileY,'linewidth',3) ; hold on ; plot(Struct.ProfileYFitted,'k--') 
set(gca,'Fontsize',14) ; title('Vertical projection')
xlabel('# pixel') ; ylabel('Intensity') ; xlim([0  Struct.width_max])

Struct.SigmaX = readattribute([devAna '/XProjFitSigma']);
Struct.SigmaY = readattribute([devAna '/YProjFitSigma']);

prov = tango_read_attribute2(devVG,'image');
Struct.image            = prov.value' ;
pause(2);
%% save other parameters
Struct.emittanceX       = readattribute([devemit '/EmittanceH']);
Struct.emittanceZ       = readattribute([devemit '/EmittanceV']);
Struct.current          = getdcct ;
Nu = gettuneFBT;
Struct.Nux              = Nu(1) ;
Struct.Nuz              = Nu(2) ;
Struct.TimeStamp        = datestr(clock);

% save
if ArchiveProfilesFlag
    save(FileName2, 'Struct');
end

disp('OK')
