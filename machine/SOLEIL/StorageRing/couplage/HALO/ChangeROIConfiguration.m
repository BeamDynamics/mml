function ChangeROIConfiguration(ConfigurationByDefault)

%% Choose here parameters

ConfigurationByDefault  = 1 ;                   % 1= configuration by default ; else = full ROI configuration
SpecificName            = 'lowalpha';  %'HU44_TEMPO' ;
ArchiveProfilesFlag     = 1;                    % 1=Archive profiles
Struct.devName                 = 'ANS-C16/DG/' ;

%% Initialization

DirectoryName   = [ getfamilydata('Directory', 'PINHOLE') 'HALO/']
FileName1       = [DirectoryName, prependtimestamp('Saving_previous_ROI_parameters')];
FileName2       = [DirectoryName, prependtimestamp('New_ROI_parameters_'), SpecificName]

% Starting time
t0 = clock;

Struct.height_max      = 494 ; % nb pixels max camera
Struct.width_max       = 659 ; % nb pixels

%% Define devices

devVG           = [Struct.devName 'PHC-VG'];
devAna          = [Struct.devName 'PHC-IMAGEANALYZER'];
devemit         = [Struct.devName 'PHC-EMIT'];

%% Save present ROI + fit parameters

Initial.origin_x         = readattribute([devVG '/roiX']);
Initial.origin_y         = readattribute([devVG '/roiY']);
Initial.width            = readattribute([devVG '/roiWidth']);
Initial.height           = readattribute([devVG '/roiHeight']);
prov=tango_get_property(devAna,'AutoROIMagFactorX') ;
Initial.ROIMagFactorX      = str2num(prov.value{:}) ;
prov=tango_get_property(devAna,'AutoROIMagFactorY') ;
Initial.ROIMagFactorY      = str2num(prov.value{:}) ;
Initial.TimeStamp          = datestr(clock);

if ConfigurationByDefault
    %% ConfigurationByDefault values
    %% Set ROI by Default
    if strcmp(Struct.devName,'ANS-C16/DG/')
        Struct.origin_x = 216 ; Struct.origin_y = 56 ; Struct.width = 208 ; Struct.height = 277 ; % Initialial values of videograber
    elseif strcmp(Struct.devName,'ANS-C02/DG/')
        Struct.origin_x = 288 ; Struct.origin_y = 62 ; Struct.width = 208 ; Struct.height = 277 ; % Initialial values of videograber
    else
        disp('Problemo with # pinhole !!')
        return
    end
    if strcmp(Struct.devName,'ANS-C16/DG/') & Initial.origin_x==216 & Initial.origin_y==56 & Initial.width==208 & Initial.height==277 | ...
            strcmp(Struct.devName,'ANS-C02/DG/') & Initial.origin_x==288 & Initial.origin_y==62 & Initial.width==208 & Initial.height==277  %%
        disp('You already get the default configuration')
    else
        
        tango_command_inout2(devVG, 'Stop');
        %---------------------
        tango_command_inout2(devVG, 'SetROI', [uint32(Struct.origin_x), uint32(Struct.origin_y), uint32(Struct.width), uint32(Struct.height)]);
        %---------------------
        tango_command_inout2(devVG, 'Start');
        
        pause(2);
        save(FileName1, 'Initial');
        disp('You change configuration to the default configuration');

    end
    %% Set ConfigurationByDefault fit parameters
    
    Struct.FactorX = 5 ;
    Struct.FactorY = 1.5 ;
    writeattribute([devAna '/AutoROIMagFactorX'],Struct.FactorX);
    writeattribute([devAna '/AutoROIMagFactorY'],Struct.FactorY);
    pause(2)
    
else
    %% new values
    %% Set full ROI
    Struct.origin_x = 0 ; Struct.origin_y = 0 ; Struct.width = Struct.width_max ; Struct.height = Struct.height_max ;
    if Initial.origin_x==0 & Initial.origin_y==0 & Initial.width==Struct.width_max & Initial.height==Struct.height_max
        disp('You already get the full ROI configuration');
    else
        
        tango_command_inout2(devVG, 'Stop');
        %---------------------
        tango_command_inout2(devVG, 'ResetROI') ;       
        %---------------------
        tango_command_inout2(devVG, 'Start');
        
        pause(2);
        save(FileName1, 'Initial');
        disp('You change configuration to the full ROI configuration');

    end
    %% Change fit parameters
    
    Struct.FactorX = 20 ;
    Struct.FactorY = 50 ;
    writeattribute([devAna '/AutoROIMagFactorX'],Struct.FactorX);
    writeattribute([devAna '/AutoROIMagFactorY'],Struct.FactorY);
    pause(2)
    
end

%% save other parameters
Struct.TimeStamp        = datestr(clock);

% save
if ArchiveProfilesFlag
    save(FileName2, 'Struct');
end

disp('OK')
