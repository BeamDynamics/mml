function rampRFphase(Voltage)

PhasePause = 0.25; 
SRRFPhaseWrite = 'ANS/RF/MAO-DISTRI_RF/channel5';

Voltage0 = readattribute(SRRFPhaseWrite);

StepVoltage = 0.1;
DiffVoltage = Voltage - Voltage0;

Nstep = floor(abs(DiffVoltage)/StepVoltage);

StepVoltage = StepVoltage*sign(DiffVoltage);


for ik = 1:Nstep,
    SetValue = Voltage0 + StepVoltage*ik;
    fprintf('RF phase Voltage %f.2 (%d/%d)\n', SetValue,ik, Nstep);
    writeattribute(SRRFPhaseWrite,SetValue);
    pause(PhasePause);
end

%final value
writeattribute(SRRFPhaseWrite,Voltage);
fprintf('RF phase Voltage %f.2 (final)\n', Voltage);

    
    
