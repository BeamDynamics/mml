%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display tables evolution
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function xbpm_display_tables(xbpm_param)

fprintf('Displaying tables... \n')
rep_name            =   '/usr/Local/configFiles/DIAG/XBPM/';

% Load ID and XBPM configuration
if (strcmp(xbpm_param.ID,'all_IDs')==1)
    nb_IDs          =   6;
else
    nb_IDs          =   1;
end
ID_name             =   {'cristal','galaxies','px1','px2','swing','sixs'}
for index=1:nb_IDs
    if (nb_IDs==6)
        xbpm_param.ID       =   ID_name{index};
    end
    XBPM_config             =   xbpm_config(xbpm_param);

% Load first data for XBPM.1
    % K table
file_name       =   strcat(rep_name,'TDL-I',XBPM_config.cell,'-',XBPM_config.section,'_DG_XBPM.1/TDL-I',XBPM_config.cell,'-',XBPM_config.section,'-XBPM1-K_Table.txt');
fid             =   fopen(file_name);
dummy           =   textscan(fid,'%s',1,'delimiter','\n');
dummy           =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines        =   str2num(char(dummy{1}));
names           =   textscan(fid,'%s',1,'delimiter','\n');
data.K1_new     =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);
    % Offset table
file_name           =   strcat(rep_name,'TDL-I',XBPM_config.cell,'-',XBPM_config.section,'_DG_XBPM.1/TDL-I',XBPM_config.cell,'-',XBPM_config.section,'-XBPM1-Offset_Table.txt');
fid                 =   fopen(file_name);
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines            =   str2num(char(dummy{1}));
names               =   textscan(fid,'%s',1,'delimiter','\n');
data.offset1_new    =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);

% Load first data for XBPM.2
    % K table
file_name       =   strcat(rep_name,'TDL-I',XBPM_config.cell,'-',XBPM_config.section,'_DG_XBPM.2/TDL-I',XBPM_config.cell,'-',XBPM_config.section,'-XBPM2-K_Table.txt');
fid             =   fopen(file_name);
dummy           =   textscan(fid,'%s',1,'delimiter','\n');
dummy           =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines        =   str2num(char(dummy{1}));
names           =   textscan(fid,'%s',1,'delimiter','\n');
data.K2_new     =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);
    % Offset table
file_name           =   strcat(rep_name,'TDL-I',XBPM_config.cell,'-',XBPM_config.section,'_DG_XBPM.2/TDL-I',XBPM_config.cell,'-',XBPM_config.section,'-XBPM2-Offset_Table.txt');
fid                 =   fopen(file_name);
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines            =   str2num(char(dummy{1}));
names               =   textscan(fid,'%s',1,'delimiter','\n');
data.offset2_new    =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);


% Load OLD data for XBPM.1
if (xbpm_param.data_nb==2)
    % K table
[file_name_temp,rep_name] = uigetfile('*.txt','Select OLD K table for XBPM.1','/usr/Local/configFiles/DIAG/XBPM/')
file_name           =   strcat(rep_name,file_name_temp);
fid                 =   fopen(file_name);
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines            =   str2num(char(dummy{1}));
names               =   textscan(fid,'%s',1,'delimiter','\n');
data.K1_old         =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);
    % Offset table
[file_name_temp,rep_name] = uigetfile('*.txt','Select OLD Offset table for XBPM.1','/usr/Local/configFiles/DIAG/XBPM/')
file_name           =   strcat(rep_name,file_name_temp);
fid                 =   fopen(file_name);
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines            =   str2num(char(dummy{1}));
names               =   textscan(fid,'%s',1,'delimiter','\n');
data.offset1_old         =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);

% Load OLD data for XBPM.2
    % K table
[file_name_temp,rep_name] = uigetfile('*.txt','Select OLD K table for XBPM.2','/usr/Local/configFiles/DIAG/XBPM/')
file_name           =   strcat(rep_name,file_name_temp);
fid                 =   fopen(file_name);
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines            =   str2num(char(dummy{1}));
names               =   textscan(fid,'%s',1,'delimiter','\n');
data.K2_old         =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);
    % Offset table
[file_name_temp,rep_name] = uigetfile('*.txt','Select OLD Offset table for XBPM.2','/usr/Local/configFiles/DIAG/XBPM/')
file_name           =   strcat(rep_name,file_name_temp);
fid                 =   fopen(file_name);
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
dummy               =   textscan(fid,'%s',1,'delimiter','\n');
nb_lines            =   str2num(char(dummy{1}));
names               =   textscan(fid,'%s',1,'delimiter','\n');
data.offset2_old         =   fscanf(fid,'%g %g %g \n',[3 nb_lines])';
fclose(fid);
end


% Choose scale for display
gap_lim         =   [5;20];
K_lim           =   [0;4];
offset_lim      =   [-0.5;0.5];
font_size       =   12;

% Display results
figure(index);clf;
set(gcf,'Position',[500 10 800 600]);
%
subplot(2,2,1) 
if (xbpm_param.data_nb==2)
    plot(data.K1_old(:,1),data.K1_old(:,2),':*r');hold on;
    plot(data.K1_old(:,1),data.K1_old(:,3),':*b');hold on;
end
plot(data.K1_new(:,1),data.K1_new(:,2),'-or','MarkerFaceColor','r');hold on;
plot(data.K1_new(:,1),data.K1_new(:,3),'-ob','MarkerFaceColor','b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(K_lim)
set(gca,'FontSize',font_size)
if (xbpm_param.data_nb==1)
    legend('x','z','Location','Best')
elseif (xbpm_param.data_nb==2)
    legend('x-old','z-old','x-new','z-new','Location','Best')
end
xlabel('Gap [mm]')
ylabel('K [-]')
title_name  =   strcat(XBPM_config.name,' - K for XBPM.1');
title(title_name)
%
subplot(2,2,2) 
if (xbpm_param.data_nb==2)
    plot(data.K2_old(:,1),data.K2_old(:,2),':*r');hold on;
    plot(data.K2_old(:,1),data.K2_old(:,3),':*b');hold on;
end
plot(data.K2_new(:,1),data.K2_new(:,2),'-or','MarkerFaceColor','r');hold on;
plot(data.K2_new(:,1),data.K2_new(:,3),'-ob','MarkerFaceColor','b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(K_lim)
set(gca,'FontSize',font_size)
if (xbpm_param.data_nb==1)
    legend('x','z','Location','Best')
elseif (xbpm_param.data_nb==2)
    legend('x-old','z-old','x-new','z-new','Location','Best')
end
xlabel('Gap [mm]')
ylabel('K [-]')
title_name  =   strcat(XBPM_config.name,' - K for XBPM.2');
title(title_name)
%
subplot(2,2,3) 
if (xbpm_param.data_nb==2)
    plot(data.offset1_old(:,1),data.offset1_old(:,2),':*r');hold on;
    plot(data.offset1_old(:,1),data.offset1_old(:,3),':*b');hold on;
end
plot(data.offset1_new(:,1),data.offset1_new(:,2),'-or','MarkerFaceColor','r');hold on;
plot(data.offset1_new(:,1),data.offset1_new(:,3),'-ob','MarkerFaceColor','b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(offset_lim)
set(gca,'FontSize',font_size)
if (xbpm_param.data_nb==1)
    legend('x','z','Location','Best')
elseif (xbpm_param.data_nb==2)
    legend('x-old','z-old','x-new','z-new','Location','Best')
end
xlabel('Gap [mm]')
ylabel('Offset [mm]')
title_name  =   strcat(XBPM_config.name,' - Offset for XBPM.1');
title(title_name)
%
subplot(2,2,4) 
if (xbpm_param.data_nb==2)
    plot(data.offset2_old(:,1),data.offset2_old(:,2),':*r');hold on;
    plot(data.offset2_old(:,1),data.offset2_old(:,3),':*b');hold on;
end
plot(data.offset2_new(:,1),data.offset2_new(:,2),'-or','MarkerFaceColor','r');hold on;
plot(data.offset2_new(:,1),data.offset2_new(:,3),'-ob','MarkerFaceColor','b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(offset_lim)
set(gca,'FontSize',font_size)
if (xbpm_param.data_nb==1)
    legend('x','z','Location','Best')
elseif (xbpm_param.data_nb==2)
    legend('x-old','z-old','x-new','z-new','Location','Best')
end
xlabel('Gap [mm]')
ylabel('Offset [mm]')
title_name  =   strcat(XBPM_config.name,' - Offset for XBPM.2');
title(title_name)

end

fprintf('Tables displayed. \n')
