function varargout = xbpm_display_tables_GUI(varargin)
% XBPM_DISPLAY_TABLES_GUI M-file for xbpm_display_tables_GUI.fig
%      XBPM_DISPLAY_TABLES_GUI, by itself, creates a new XBPM_DISPLAY_TABLES_GUI or raises the existing
%      singleton*.
%
%      H = XBPM_DISPLAY_TABLES_GUI returns the handle to a new XBPM_DISPLAY_TABLES_GUI or the handle to
%      the existing singleton*.
%
%      XBPM_DISPLAY_TABLES_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in XBPM_DISPLAY_TABLES_GUI.M with the given input arguments.
%
%      XBPM_DISPLAY_TABLES_GUI('Property','Value',...) creates a new XBPM_DISPLAY_TABLES_GUI or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before xbpm_display_tables_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to xbpm_display_tables_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help xbpm_display_tables_GUI

% Last Modified by GUIDE v2.5 21-Sep-2017 14:52:32

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @xbpm_display_tables_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @xbpm_display_tables_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before xbpm_display_tables_GUI is made visible.
function xbpm_display_tables_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to xbpm_display_tables_GUI (see VARARGIN)

% Choose default command line output for xbpm_display_tables_GUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);

% UIWAIT makes xbpm_display_tables_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = xbpm_display_tables_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset the data.
if isfield(handles, 'metricdata') && ~isreset
    return;
end

set(handles.IDgroup, 'SelectedObject', handles.cristal);
set(handles.data_nb, 'SelectedObject', handles.current);
%
handles.ID          =   'cristal';
handles.data_nb     =   1;

% Update handles structure
guidata(handles.figure1, handles);


% --- Executes when selected object is changed in IDgroup.
function IDgroup_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in IDgroup 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

if (hObject == handles.cristal)
    fprintf('CRISTAL selected \n')
    handles.ID='cristal';
elseif (hObject == handles.galaxies)
    fprintf('GALAXIES selected \n')
    handles.ID='galaxies';
elseif (hObject == handles.px1)
    fprintf('PX1 selected \n')
    handles.ID='px1';    
elseif (hObject == handles.px2)
    fprintf('PX2 selected \n')
    handles.ID='px2';    
elseif (hObject == handles.nano)
    fprintf('NANO selected \n')
    handles.ID='nano';
elseif (hObject == handles.tomo)
    fprintf('TOMO selected \n')
    handles.ID='tomo';    
elseif (hObject == handles.sixs)
    fprintf('SIXS selected \n')
    handles.ID='sixs';    
elseif (hObject == handles.swing)
    fprintf('SWING selected \n')
    handles.ID='swing'; 
elseif (hObject == handles.all_IDs)
    fprintf('All IDs selected \n')
    handles.ID='all_IDs'; 
else
    fprintf('!!!!!!!!!!!!!! Error in ID selection !!!!!!!!!!!!! \n')
end

% Update handles structure
guidata(handles.figure1, handles);


% --- Executes when selected object is changed in data_nb.
function data_nb_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in data_nb 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)

if (hObject == handles.current)
    fprintf('Only current tables selected \n')
    handles.data_nb=1;
elseif (hObject == handles.current_old)
    fprintf('Current + Old tables selected \n')
    handles.data_nb=2;  
end

% Update handles structure
guidata(handles.figure1, handles);

% --- Executes on button press in display.
function display_Callback(hObject, eventdata, handles)
% hObject    handle to display (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xbpm_param.ID       =   handles.ID;
xbpm_param.xbpm_nb  =   1;
xbpm_param.data_nb  =   handles.data_nb;
xbpm_param.comment  =   '';
xbpm_param.rep      =   '';
%
xbpm_display_tables(xbpm_param);
