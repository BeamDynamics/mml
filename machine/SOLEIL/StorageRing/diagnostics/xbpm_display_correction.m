%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Display last measurements
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function xbpm_display_correction()

fprintf('Displaying current correction... \n')

% Load data
    % CRISTAL
[file_name_temp,rep_name]   = uigetfile('*.txt','Select data for CRISTAL','/home/data/DG/XBPM/XBPMs-TDL/TDL-I06-C_DG_XBPM.1/')
file_name                   =   strcat(rep_name,file_name_temp);
data_cristal                =   load(file_name);
    % GALAXIES
[file_name_temp,rep_name]   = uigetfile('*.txt','Select data for GALAXIES','/home/data/DG/XBPM/XBPMs-TDL/TDL-I07-C_DG_XBPM.1/')
file_name                   =   strcat(rep_name,file_name_temp);
data_galaxies   =   load(file_name);
    % PX1
[file_name_temp,rep_name]   = uigetfile('*.txt','Select data for PX1','/home/data/DG/XBPM/XBPMs-TDL/TDL-I10-C_DG_XBPM.1/')
file_name                   =   strcat(rep_name,file_name_temp);
data_px1        =   load(file_name);
    % PX2
[file_name_temp,rep_name]   = uigetfile('*.txt','Select data for PX2','/home/data/DG/XBPM/XBPMs-TDL/TDL-I11-M_DG_XBPM.1/')
file_name                   =   strcat(rep_name,file_name_temp);
data_px2        =   load(file_name);
    % SWING
[file_name_temp,rep_name]   = uigetfile('*.txt','Select data for SWING','/home/data/DG/XBPM/XBPMs-TDL/TDL-I11-C_DG_XBPM.1/')
file_name                   =   strcat(rep_name,file_name_temp);
data_swing      =   load(file_name);
    % SIXS
[file_name_temp,rep_name]   = uigetfile('*.txt','Select data for SIXS','/home/data/DG/XBPM/XBPMs-TDL/TDL-I14-C_DG_XBPM.1/')
file_name                   =   strcat(rep_name,file_name_temp);
data_sixs       =   load(file_name);

% Choose scale for display
gap_lim         =   [5;20];
y_lim           =   [-0.1;0.1];
font_size       =   10;

% Display results
figure(1);clf;
set(gcf,'Position',[500 10 1000 1000]);
%
subplot(6,2,1)
plot(data_cristal(:,1),data_cristal(:,2),'-*r');hold on;
plot(data_cristal(:,1),data_cristal(:,3),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('Cristal - XBPM.1')
%
subplot(6,2,2)
plot(data_cristal(:,1),data_cristal(:,4),'-*r');hold on;
plot(data_cristal(:,1),data_cristal(:,5),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('Cristal - XBPM.2')
%
subplot(6,2,3)
plot(data_galaxies(:,1),data_galaxies(:,2),'-*r');hold on;
plot(data_galaxies(:,1),data_galaxies(:,3),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('Galaxies - XBPM.1')
%
subplot(6,2,4)
plot(data_galaxies(:,1),data_galaxies(:,4),'-*r');hold on;
plot(data_galaxies(:,1),data_galaxies(:,5),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('Galaxies - XBPM.2')
%
subplot(6,2,5)
plot(data_px1(:,1),data_px1(:,2),'-*r');hold on;
plot(data_px1(:,1),data_px1(:,3),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('PX1 - XBPM.1')
%
subplot(6,2,6)
plot(data_px1(:,1),data_px1(:,4),'-*r');hold on;
plot(data_px1(:,1),data_px1(:,5),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('PX1 - XBPM.2')
%
subplot(6,2,7)
plot(data_px2(:,1),data_px2(:,2),'-*r');hold on;
plot(data_px2(:,1),data_px2(:,3),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('PX2 - XBPM.1')
%
subplot(6,2,8)
plot(data_px2(:,1),data_px2(:,4),'-*r');hold on;
plot(data_px2(:,1),data_px2(:,5),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('PX2 - XBPM.2')
%
subplot(6,2,9)
plot(data_swing(:,1),data_swing(:,2),'-*r');hold on;
plot(data_swing(:,1),data_swing(:,3),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('Swing - XBPM.1')
%
subplot(6,2,10)
plot(data_swing(:,1),data_swing(:,4),'-*r');hold on;
plot(data_swing(:,1),data_swing(:,5),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
ylabel('Position [mm]')
title('Swing - XBPM.2')
%
subplot(6,2,11)
plot(data_sixs(:,1),data_sixs(:,2),'-*r');hold on;
plot(data_sixs(:,1),data_sixs(:,3),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
xlabel('Gap [mm]')
ylabel('Position [mm]')
title('Sixs - XBPM.1')
%
subplot(6,2,12)
plot(data_sixs(:,1),data_sixs(:,4),'-*r');hold on;
plot(data_sixs(:,1),data_sixs(:,5),'-*b');hold on;
hold off;
grid on;
xlim(gap_lim)
ylim(y_lim)
set(gca,'FontSize',font_size)
legend('x','z','Location','East')
xlabel('Gap [mm]')
ylabel('Position [mm]')
title('Sixs - XBPM.2')