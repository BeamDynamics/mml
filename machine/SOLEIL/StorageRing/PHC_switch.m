function switch_phc_config(varargin)
%
%
%  Optional input arguments
%  . 'Get'    - get PHC param from devices and save in file.
%  . 'Set'    - get PHC param from file and set in device.
%  . 'NoGet'    - get PHC param from devices and save in file.
%  . 'NoSet'    - get PHC param from file and set in device.
%  . 'GetGolden'  - Get PHC param from devices and save in GoldenFile.
%  . 'SetGolden'  - Get PHC param from GoldenFile and set in device.
%  . 'PHC_List' + value  - if you want only set some PHC ex:  switch_phc_config('Set','PHC_List',{'ANS-C02/DG/PHC-EMIT';'ANS-C16/DG/PHC-EMIT'})
%  . 'Attr_List' + value  - if you want only set some attribute of PHC ex:  switch_phc_config('Set','Attr_List',attr_list = {'AlphaX','AlphaZ'})
%  
%
%  EXAMPLES
%  1. standard 
%      switch_phc_config;
%  2. to set PHC devices to Golden Values
%      switch_phc_config('SetGolden');
%  3. to get PHC devices values and set in GoldenFile
%      switch_phc_config('GetGolden');
%

%
%  Written by A.Bence
%%
%% to rename switch_phc_config


varargin2     = {};
Prefix_GOLDEN_file='PHC-Config-Golden_';

% put Flag to default setup
DisplayFLAG = 1;
SetFLAG = 1;
GetFLAG = 1;
GetGoldentFLAG = 0;
SetGoldentFLAG = 0;

% default list of devices and attributes
%dev_list = {'ANS-C02/DG/PHC-EMIT';'ANS-C04/DG/PHC-EMIT';'ANS-C16/DG/PHC-EMIT'};
dev_list = {'ANS-C02/DG/PHC-EMIT';'ANS-C16/DG/PHC-EMIT'};
attr_list = {'AlphaX','AlphaZ','BetaX','BetaZ','EtaX','EtaZ','EtaPX','EtaPZ'};

%flatten cellarray if you give all argins in one variable
for i = length(varargin):-1:1    
    if iscell(varargin{i})
        varargin=[varargin{:}];
    end    
end    

for i = length(varargin):-1:1    
    if strcmpi(varargin{i},'Get')
        GetFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Set')
        SetFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'NoGet')
        GetFLAG = 0;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'NoSet')
        SetFLAG = 0;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];    
    elseif strcmpi(varargin{i},'GetGolden')
        GetGoldenFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
    elseif strcmpi(varargin{i},'SetGolden')
        SetGoldenFLAG = 1;
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];   
    elseif strcmpi(varargin{i},'PHC_List')
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
        dev_list=varargin(i);
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Attr_List')
        varargin2 = {varargin2{:} varargin{i}};
        varargin(i) = [];
        attr_list=varargin(i);
        varargin(i) = [];
    end    
end 

%%

if GetFLAG || GetGoldenFLAG
    for j=1:length(dev_list)   
        devemit = dev_list{j};
        disp(strcat('Read PHC device: ',devemit));
        %clean_name=strrep(strrep(devemit,'/','_'),'-','_');
        PHC(j).name=devemit;
        for i=1:length(attr_list)
            temp = tango_read_attribute2(devemit,attr_list{i});
            PHC(j).(attr_list{i})=temp;
        end   
    end

    if DisplayFLAG         
        disp('Before modification')       
        for ik =1:length(attr_list),
            fprintf('%10s PHC1 =%+8.04f PHC3 = %+8.04f\n', attr_list{ik}, ...
                PHC(1).(attr_list{ik}).value(1), PHC(2).(attr_list{ik}).value(1));
        end        
    end

    if GetFLAG
         FileName=strcat(appendtimestamp('Conf_PHCs'),'.mat');
         PathPHC=getfamilydata('Directory','PINHOLE');
         fullpath=fullfile(PathPHC,'conf_phc',FileName);
         [filename, pathname]=uiputfile(fullpath,'Sauvegarde de la conf PHC 1 & 3 actuel sinon faite Cancel');
    elseif GetGoldenFLAG
         FileName = getfamilydata('OpsData','LatticeFile');
         [DirectoryName, FileName, Ext, VerNumber] = fileparts(FileName);
         if isempty(DirectoryName)
               DirectoryName = getfamilydata('Directory', 'OpsData');
         end
         pathname=DirectoryName
         filename=[Prefix_GOLDEN_file,FileName, '.mat']; 
    else
         filename=[];
         pathname=[];
    end    
    if isequal(filename,0) || isequal(pathname,0)
          disp('User pressed cancel: no configuration saved')       
    else
        save(fullfile(pathname,filename),'PHC');
    end

end    

 
 
 
 if SetFLAG
    [filename, pathname]=uigetfile(fullfile(getfamilydata('Directory','PINHOLE'),'conf_phc'),'Chargement de la conf PHC 1 & 3 dans les devices');
 elseif SetGoldenFLAG
      FileName = getfamilydata('OpsData','LatticeFile');
      [DirectoryName, FileName, Ext, VerNumber] = fileparts(FileName);
      if isempty(DirectoryName)
               DirectoryName = getfamilydata('Directory', 'OpsData');
      end
      FullpathSetGolden = fullfile(DirectoryName,[Prefix_GOLDEN_file,FileName, '.mat']);        
      if ~exist(FullpathSetGolden, 'file') == 2
        SetGoldenFLAG=0;
        disp('this lattice have no PHC GoldenFile\n')
      end
     [pathname,filename,EXT,VERSN]=fileparts( FullpathSetGolden);
     filename=[filename,EXT];
 else 
     filename=[];
     pathname=[];
 end   
 if isequal(filename,0) || isequal(pathname,0)
        disp('User pressed cancel: no configuration loaded')       
 else
        load(fullfile(pathname,filename));
        for j=1:length(dev_list)
            devemit = dev_list{j};
            disp(strcat('modification de :',devemit));
            for i=1:length(attr_list)
                tango_write_attribute2(devemit,attr_list{i},PHC(j).(attr_list{i}).value(1));    
            end
        end
        disp('PHC modifié'); 
        if GetFLAG
            fprintf('After modification (file %s)\n', filename)
            for ik =1:length(attr_list),
                fprintf('%10s PHC1 =%+8.04f PHC2 = %+8.04f\n', attr_list{ik}, ...
                PHC(1).(attr_list{ik}).value(1), PHC(2).(attr_list{ik}).value(1));
            end
        end
 end  

 
