%% Data from maher Warning in AT the alpha1 and apha 2 value are not the same
 Cavity voltage = 2MV

  Alpha1     fs(Hz)   bucket half-height(%)  Alpha2 critical   Alpha2    natural sigmal(mm)  fs-turns  
  ------     ------   -------------------    ---------------   ------    ---------------     --------
 4.488E-5   1168.87           9.6               2.699e-4      1.798E-5       1.865             724.3
 2.193E-5    817.51          13.74              9.217E-5      2.050E-5       1.305            1035.6 
 1.052E-5    566.66          19.83              3.062E-5      9.586E-6       0.904            1494.1  
 7.424E-6    476.40          23.61              1.815E-5      3.862E-6       0.760            1777.1
 5.549E-6    412.14          27.31              1.173E-5      2.205E-6       0.658            2054.2  
 4.300E-6    363.12          31.03              8.000E-6      4.006E-6       0.580            2331.5
 3.707E-6    337.38          33.41              6.405E-6      2.938E-6       0.538            2509.4 
 3.033E-6    305.55          36.94              4.741E-6      1.477E-6       0.488            2770.8
 1.003E-6    177.62          64.24              9.014E-7      2.657E-7       0.284            2766.4

 %% AT values (below), Maher Values (above)

 Cavity voltage = 2MV

 #   Alpha1   fs(Hz)   bucket half-height(%)    Alpha2    natural sigmal(mm)  fs-turns  
 -  ------    ------   -------------------       ------    ---------------     --------
 1  4.5E-5     1177           9.7              -6.4E-5       1.85            718
 2  2.2E-5      825          13.8              -5.7E-5       1.29            1026
 3  1.1E-5      573          19.9              -8.1E-5       0.90            1476  
 4  7.7E-6      483          23.6              -8.7E-5       0.76            1751
 5  5.8E-6      419          27.3              -9.3E-5       0.66            2020  
 6  4.4E-6      367          31.1              -9.0E-5       0.57            2309
 7  3.9E-6      343          33.3              -9.3E-5       0.53            2469
 8  3.3E-6      315          36.2              -9.7E-5       0.60            2686
 9  1.3E-6      194          58.6              -9.2E-5       0.30            4343
10  1.0E-6      174          65.5              -9.5E-5       0.30            4854

%% File for low alpha lattices in Physics units

%Alpha1   Alpha2        Q1       Q2         Q3        Q4      Q5         Q6       Q7        Q8        Q44      Q55        SX1         SX2       SX3        SX4       SX5        SX6        SX7        SX8        SX33      SX44
%------   ------     -------    -----     ------    ------   ------    ------   -------   ------    -------   ------   ----------  --------   ---------  -------   --------   --------   -------   --------    --------   --------
R = [
4.488E-5  1.798E-5  -0.823331  1.59795  -0.988722  -1.14089  1.88396  -1.58517  1.17045  0.936830  -1.46865  2.02185  -0.458717E7  -1.700E7  -3.73577E7  4.505E7  -2.20850E7  2.40098E7  -2.000E7  2.15625E7  -3.56698E7  4.1600E7
2.193E-5  2.050E-5  -0.820384  1.59859  -1.001960  -1.13805  1.88819  -1.58734  1.16966  0.934607  -1.46687  2.03139  -0.636492E7  -1.700E7  -3.86155E7  4.502E7  -2.31976E7  2.51579E7  -2.000E7  2.18828E7  -3.60938E7  4.4000E7
1.052E-5  9.586E-6  -0.820132  1.59895  -1.005240  -1.13678  1.89044  -1.58889  1.16934  0.933384  -1.46567  2.03619  -0.647964E7  -1.900E7  -3.86842E7  4.600E7  -2.27978E7  2.49070E7  -2.100E7  2.16542E7  -3.67007E7  4.4820E7
7.424E-6  3.862E-6  -0.820464  1.59920  -1.005780  -1.13582  1.89095  -1.59024  1.16982  0.932500  -1.46448  2.03737  -0.702534E7  -1.900E7  -3.84373E7  4.410E7  -2.28146E7  2.50031E7  -2.100E7  2.21791E7  -3.68537E7  4.6000E7
5.549E-6  2.205E-6  -0.825471  1.60140  -1.003400  -1.13440  1.89134  -1.59366  1.17404  0.927200  -1.46192  2.03765  -0.777599E7  -1.400E7  -3.81059E7  4.500E7  -2.34820E7  2.55077E7  -2.100E7  2.21535E7  -3.83024E7  4.6000E7
4.300E-6  4.006E-6  -0.846563  1.61548  -1.017480  -1.09416  1.88471  -1.65341  1.24670  0.842562  -1.41338  2.03177  -0.611428E7  -1.900E7  -4.11955E7  4.900E7  -2.17347E7  2.44284E7  -1.950E7  2.09971E7  -3.83507E7  4.6150E7
3.707E-6  2.938E-6  -0.834576  1.60844  -1.014770  -1.11005  1.88724  -1.62517  1.20081  0.898442  -1.43284  2.03431  -0.636947E7  -1.600E7  -3.88549E7  4.700E7  -2.22207E7  2.45436E7  -2.100E7  2.17696E7  -3.92876E7  4.6080E7
3.033E-6  1.477E-6  -0.821701  1.59892  -1.001130  -1.14792  1.89420  -1.57075  1.14447  0.960216  -1.47870  2.04087  -0.632823E7  -1.900E7  -3.91032E7  4.600E7  -2.38489E7  2.49108E7  -2.100E7  2.16293E7  -3.61675E7  4.5040E7
1.003E-6  8.167E-7  -0.814298  1.59352  -0.993900  -1.16605  1.89793  -1.54398  1.11866  0.987108  -1.50116  2.04488  -0.773654E7  -1.700E7  -3.82168E7  4.300E7  -2.50207E7  2.56321E7  -2.100E7  2.23040E7  -3.57030E7  4.5905E7
7.462e-7  1.321e-7  -0.797821  1.58655  -1.006290  -1.16815  1.89785  -1.53478  1.10566  1.002440  -1.50723  2.04637  -0.768908E7  -1.800E7  -3.78153E7  4.300E7  -2.43550E7  2.59915E7  -2.12233E7 2.25286E7  -3.56100E7  4.5814E7
4.300E-6  4.006E-6  -0.846563  1.61548  -1.017480  -1.09416  1.88471  -1.65341  1.24670  0.842562  -1.41338  2.03177  -0.611428E7  -1.900E7  -4.11955E7  4.900E7  -2.17347E7  2.44284E7  -1.950E7  2.09971E7  -3.83507E7  4.6260E7
2.030e-5  1.630E-6  -0.827994  1.65322  -1.027318  -1.14911  1.88293  -1.59052  1.17174  0.938558  -1.47324  2.01812  -0.369830E7  -1.700E7  -3.67288E7  4.505E7  -1.91834E7  2.29247E7  -2.000E7  2.14024E7  -3.54578E7  4.0400E7
];

% scaling with respect to line 2 (alpha1/20)
%dRoR = (R - repmat(R(12,:),12,1))./repmat(R(2,:),12,1); Bug sur R12
dRoR = (R - repmat(R(2,:),12,1))./repmat(R(2,:),12,1);

%Mode and unit selection
Mode  = 'Online';
%Units = 'Hardware';
Units = 'Physics';

% starting point: this is the reference for the scaling factor.
% All the quads
K1i_before  = getsp('Q1',Mode, Units);
K2i_before  = getsp('Q2',Mode, Units);
K3i_before  = getsp('Q3',Mode, Units);
K4i_before  = getsp('Q4',Mode, Units);
K5i_before  = getsp('Q5',Mode, Units);
K6i_before  = getsp('Q6',Mode, Units);
K7i_before  = getsp('Q7',Mode, Units);
K8i_before  = getsp('Q8',Mode, Units);
K9i_before  = getsp('Q9',Mode, Units);
K10i_before = getsp('Q10',Mode, Units);

% All the sextupoles
S1i_before  = getsp('S1',Mode, Units);
S2i_before  = getsp('S2',Mode, Units);
S3i_before  = getsp('S3',Mode, Units);
S4i_before  = getsp('S4',Mode, Units);
S5i_before  = getsp('S5',Mode, Units);
S6i_before  = getsp('S6',Mode, Units);
S7i_before  = getsp('S7',Mode, Units);
S8i_before  = getsp('S8',Mode, Units);
S9i_before  = getsp('S9',Mode, Units);
S10i_before = getsp('S10',Mode, Units);

%% Selection for the scaling from refence point
% alpha1 = 2.1e-8;
% k = 1.06;
% DK = dRoR(9,3:22);

%alpha1 = -4.12e-08 alpha2 = -9.68e-05 factor = -10926  
%k = 1.05; % scaling factor factor
%DK = dRoR(10,3:22);

k = 0; % scaling factor factor
DK = dRoR(5,3:22);

setsp('Q1' ,(k* DK(1)  + 1)*K1i_before, Mode, Units);
setsp('Q2' ,(k* DK(2)  + 1)*K2i_before, Mode, Units);
setsp('Q3' ,(k* DK(3)  + 1)*K3i_before, Mode, Units);
setsp('Q4' ,(k* DK(4)  + 1)*K4i_before, Mode, Units);
setsp('Q5' ,(k* DK(5)  + 1)*K5i_before, Mode, Units);
setsp('Q6' ,(k* DK(6)  + 1)*K6i_before, Mode, Units);
setsp('Q7' ,(k* DK(7)  + 1)*K7i_before, Mode, Units);
setsp('Q8' ,(k* DK(8)  + 1)*K8i_before, Mode, Units);
setsp('Q9' ,(k* DK(9)  + 1)*K9i_before, Mode, Units);
setsp('Q10',(k* DK(10) + 1)*K10i_before, Mode, Units);

%%
setsp('S1' ,(k* DK(11)  + 1)*S1i_before, Mode, Units);
setsp('S2' ,(k* DK(12)  + 1)*S2i_before, Mode, Units);
setsp('S3' ,(k* DK(13)  + 1)*S3i_before, Mode, Units);
setsp('S4' ,(k* DK(14)  + 1)*S4i_before, Mode, Units);
setsp('S5' ,(k* DK(15)  + 1)*S5i_before, Mode, Units);
setsp('S6' ,(k* DK(16)  + 1)*S6i_before, Mode, Units);
setsp('S7' ,(k* DK(17)  + 1)*S7i_before, Mode, Units);
setsp('S8' ,(k* DK(18)  + 1)*S8i_before, Mode, Units);
setsp('S9' ,(k* DK(19)  + 1)*S9i_before, Mode, Units);
setsp('S10',(k* DK(20) + 1)*S10i_before, Mode, Units);

%%
atsummary;
A = physics_mcf('NoDisplay');
fprintf('alpha1 = %3.2e alpha2 = %3.2e factor = %4.0f \n', A(end), A(end-1), 4.5e-4/A(end));


%% Summary of the scaling from one point to the other
figure
hold on; grid on
for k =3:12, % 10 quad
    plot(R(1:end-2,1),(R(1:end-2,k)-R(1,k))./R(1,k),'.-','Color',nxtcolor);
end
xlabel('alpha1')
ylabel('Relative K variation')

%
figure
hold on; grid on
for k =13:22, % 10 sextu
    plot(R(1:end-2,1),(R(1:end-2,k)-R(1,k))./R(1,k),'.-','Color',nxtcolor);
end
xlabel('alpha1')
ylabel('Relative S variation')

%%
figure
plot(R(:,1),R(:,2)./R(:,1),'.-');
grid on
xlabel('alpha1')
ylabel('alpha2/alpha1')

%%
Rtune = meastuneresp({'Q8', 'Q9'},'NoArchive', 'Model')
Rchro = measchroresp({'S5', 'S6'},'Archive', 'Model')

%%
dI = inv(Rtune)*[0.0; 0.005]
stepsp('Q8', dI(1));stepsp('Q9', dI(2));

%%
dI = inv(Rchro)*[0.5; 0.0]
stepsp('S5', dI(1));stepsp('S6', dI(2));

%%
Rtune = gettuneresp({'Q8','Q9'});
Rchro = getchroresp({'S5', 'S6'});

%%
steptune([0.0 0], Rtune, {'Q8','Q9'})
stepchro([0 0], Rchro, {'S5','S6'})

%%

setsp('Q1' ,R(3,ik), Mode, Units);
setsp('Q2' ,(k* DK(2)  + 1)*K2i_before, Mode, Units);
setsp('Q3' ,(k* DK(3)  + 1)*K3i_before, Mode, Units);
setsp('Q4' ,(k* DK(4)  + 1)*K4i_before, Mode, Units);
setsp('Q5' ,(k* DK(5)  + 1)*K5i_before, Mode, Units);
setsp('Q6' ,(k* DK(6)  + 1)*K6i_before, Mode, Units);
setsp('Q7' ,(k* DK(7)  + 1)*K7i_before, Mode, Units);
setsp('Q8' ,(k* DK(8)  + 1)*K8i_before, Mode, Units);
setsp('Q9' ,(k* DK(9)  + 1)*K9i_before, Mode, Units);
setsp('Q10',(k* DK(10) + 1)*K10i_before, Mode, Units);

setsp('S1' ,(k* DK(11)  + 1)*S1i_before, Mode, Units);
setsp('S2' ,(k* DK(12)  + 1)*S2i_before, Mode, Units);
setsp('S3' ,(k* DK(13)  + 1)*S3i_before, Mode, Units);
setsp('S4' ,(k* DK(14)  + 1)*S4i_before, Mode, Units);
setsp('S5' ,(k* DK(15)  + 1)*S5i_before, Mode, Units);
setsp('S6' ,(k* DK(16)  + 1)*S6i_before, Mode, Units);
setsp('S7' ,(k* DK(17)  + 1)*S7i_before, Mode, Units);
setsp('S8' ,(k* DK(18)  + 1)*S8i_before, Mode, Units);
setsp('S9' ,(k* DK(19)  + 1)*S9i_before, Mode, Units);
setsp('S10',(k* DK(20) + 1)*S10i_before, Mode, Units);
