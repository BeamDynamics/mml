function str_state=Test_which_lattice(option)
%Test_which_lattice - Tests the lattice name loaded on the machine
% 
% PUMA W164 fermé @ 14.7 mm
%       Q8    [ 5,1]     -149.14004       -1.25287     -149.13949       -1.25286 
%       Q8    [ 6,1]     -149.97291       -1.25981     -149.97034       -1.25979 
% SLICING W164 fermé @ 16.7 mm
%       Q8    [ 5,1]     -152.10300       -1.27755     -152.10190
%       -1.27755 
%       Q8    [ 6,1]     -153.28100       -1.28736     -153.27941       -1.28734 
% ni l'un ni l'autre
%       Q8    [ 5,1]     -162.60600       -1.36465     -162.60498       -1.36464 
%       Q8    [ 6,1]     -163.99300       -1.37610     -163.99260       -1.37610 
%


%% Written by M.-A. Tordeux
% modified by A.Bence and L. Nadolski

str_state='';
DispFlag=1;
if exist('option', 'var') && strcmp(option,'NoDisplay')
    DispFlag=0;
end

Q8 = getam('Q8');
val = mean(Q8(7:8));

if val<151
   if DispFlag
       warndlg('Lattice is the one of PUMA, with W164 closed at 14.7mm','!! Warning !!') 
   end
    str_state=strcat(str_state,'-PUMA-');
elseif val>=151 & val<156
    if DispFlag
        warndlg('Lattice is the one of SLICING, with W164 closed at 16.7mm','!! Warning !!')
    end
    str_state=strcat(str_state,'-SLICING-');
else
    if DispFlag
        warndlg('Lattice is the one of standard Nanoscopium with W164 OPEN','!! Warning !!')
    end
    str_state=strcat(str_state,'-STD_NANO-');
end

Q9   = getam('Q9');
val2 = mean(Q9(3:4));
Q10  = getam('Q10');
val3 = mean(Q10(3:4));

if val2<-185 && val3>210
    if DispFlag 
        warndlg('Lattice is for WSV50 closed at 4.5mm','!! Warning !!')
    end
    str_state=strcat(str_state,'-WSV50_4.5mm-');
elseif val2<-158 && val3>208
    if DispFlag 
        warndlg('Lattice is for WSV50 open 40mm','!! Warning !!')
    end
    str_state=strcat(str_state,'-WSV50_40mm-');
    else
    if DispFlag 
        warndlg('Lattice is for WSV50 closed at 5.5mm','!! Warning !!')
    end
    str_state=strcat(str_state,'-WSV50_5.5mm-');
end    
