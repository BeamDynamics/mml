function data = measlifetime_scraper(targetValue)
%  measlifetime_scraper - Measure Lifetime with respect to the horizontal
%  scraper position
%
%  INPUTS:
%  1. targetValue - Tagert beam current for top-up injection
%

% Added data from all BLMs
% if OK removed data from ANS-C01/DG/BLM.2/SaC
% Better : make the BLM group in SOLEIL init

%
%% Written by Laurent S. Nadolski
DisplayFlag = 1;

%targetValue = 450; %mA

SCRAPER_startingvaldaue = getam('SCRAPER');
PressureAttributeName = 'ANS/VI/CALC-PI.1/mean';
BLMAttributeName = 'ANS-C01/DG/BLM.2/SaC'; %14 Hz

% make a group for BLM
BLMgroup=tango_group_create2('BLMs');
device_list={'ANS-C01/DG/BLM.1','ANS-C01/DG/BLM.2','ANS-C04/DG/BLM.1','ANS-C04/DG/BLM.2','ANS-C16/DG/BLM.1'};
tango_group_add(BLMgroup,device_list);


fileName = 'LifetimeScraper';

% Scraper values
scraperEndValue = -35;
scraperStartingValue = -17;
%scraperStartingValue = -27;

WaitFlag = 0.5; % s
LifetimeTime = 45; %s OK for 450 mA

scraperStepValue = -1;

scraper_value =scraperStartingValue:scraperStepValue:scraperEndValue;

% add last value if necessary
if scraper_value(end) ~= scraperEndValue
    scraper_value = [scraper_value scraperEndValue];
end

% add fully open value
scraper_value = [scraper_value -35];


len = length(scraper_value);

data.scraperh = zeros(len,1);
data.lifetime = zeros(len,1);
data.current  = zeros(len,1);
data.pressure = zeros(len,1);
data.tunes    = zeros(len,2);
data.emitx    = zeros(len,2);
data.emitz    = zeros(len,2);
data.scraper  = zeros(len,4);
data.blm      = zeros(len,1);
%data.blmdata  = zeros(len,1);
data.RFvoltage= zeros(len,4);

% Waiting till position reached
setsp('SCRAPER', scraperStartingValue,[1 3], WaitFlag);

topup_thebeam(targetValue);

[Tau, I0, t, DCCT] = measlifetime(LifetimeTime);

k =1;
data.scraperh(k) = scraper_value(k);
data.RFvoltage(k,:) = getam('CM');
data.lifetime(k) = Tau;
data.current(k)  = I0;
data.pressure(k) = readattribute(PressureAttributeName);
data.emitx(k,:)    = getam('PHCx');
data.emitz(k,:)    = getam('PHCz');
data.scraper(k,:)  = getam('SCRAPER');
data.tunes(k,:)    = gettuneFBT;

data.blm(k)        = averageBLM(LifetimeTime);

blm_struct = getAverageBLMdata(BLMgroup, LifetimeTime);
data.blmdata(k).AverageBLMdata = blm_struct.averagedValue;
data.blmdata(k).maxADC = blm_struct.maxADC;


for k =2:len,
    setsp('SCRAPER', scraper_value(k),[1 3], WaitFlag);
    [Tau, I0, t, DCCT] = measlifetime(LifetimeTime);
    data.scraperh(k)   = scraper_value(k);
    data.RFvoltage(k,:) = getam('CM');
    data.lifetime(k)   = Tau;
    data.current(k)    = I0;
    data.pressure(k)   = readattribute(PressureAttributeName);
    data.emitx(k,:)    = getam('PHCx');
    data.emitz(k,:)    = getam('PHCz');
    data.scraper(k,:)  = getam('SCRAPER');
    data.blm(k)        = averageBLM(LifetimeTime);
    data.tunes(k,:)    = gettuneFBT;
    
    blm_struct = getAverageBLMdata(BLMgroup, LifetimeTime);
    data.blmdata(k).AverageBLMdata = blm_struct.averagedValue;
    data.blmdata(k).maxADC = blm_struct.maxADC;
    
    if DisplayFlag
        fprintf('Scraper-H : %3.2f Lifetime %3.2f\n', scraper_value(k), data.lifetime(k) )
    end
    
    save([fileName, '_tmp'], 'data');
    topup_thebeam(targetValue)
end

data.TimeStamp = clock;
data.CreatedBy = mfilename;

save(appendtimestamp(fileName), 'data');

strgMessage = 'Mesures finis';
tango_giveInformationMessage(strgMessage);

% Force defined Position to -35 after movin the scraper
% do it by hand for now

if DisplayFlag
    plot(data.scraperh, data.lifetime, 'k*-');
    xlabel('Vertical scraper position (mm)')
    ylabel('Lifetime (h)')
end

    function averagedValue = averageBLM(nSeconds)
        
        % data @ 14 Hz
        
        %nSeconds = 60;
        
        nData = 14*nSeconds;
        
        %data hisotry @ 14 Hz
        tmp = tango_read_attribute2('ANS-C01/DG/BLM.2', 'SaHistoryC');
        averagedValue = mean(tmp.value(end-nData:end));
    end
end
