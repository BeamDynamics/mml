function HU256_CheckGlobalValues()
    global HU256CELL;
    global BEAMLINENAME;
    global SENSEOFCURRENT;
    global PRESENTCURRENT;
    global BXPRESENTCURRENTFORHEL;
    global BXSENSEOFCURRENTFORHEL;
    global POWERSUPPLYNAME;
    global BXLISTOFCURRENTSFORHEL;
%     global BXREFORBITFORHEL;
    global LISTOFCURRENTS;
    global REFORBIT;
%     global CORRCURRTABLE;
    global RELATIVETOLERANCE;
    global APERIODIC;
    global MAXDELAY;
    global TESTWITHOUTPS;
    global TESTWITHOUTBEAM;
    global DEBUG;
    
    fprintf('HU256CELL: %1.0f. BEAMLINENAME: %s.\n', HU256CELL, BEAMLINENAME)
    fprintf('POWERSUPPLYNAME: %s\n', POWERSUPPLYNAME)
%     LISTOFCURRENTS
%     BXLISTOFCURRENTSFORHEL
    fprintf('size(REFORBIT): %1.0f\n', size(REFORBIT))
    fprintf('SENSEOFCURRENT: %1.0f. PRESENTCURRENT: %3.3f. BXSENSEOFCURRENTFORHEL: %1.0f. BXPRESENTCURRENTFORHEL: %3.3f\n', SENSEOFCURRENT, PRESENTCURRENT, BXSENSEOFCURRENTFORHEL, BXPRESENTCURRENTFORHEL)
    