%% Building ordered lists of currents values to perform cycling
    NbBzCurrents=length(BzCurrents);
    NbBxCurrents=length(BxCurrents);
    NbBzCorrectorCurrents=length(BzCorrectorCurrents);
    NbBxCorrectorCurrents=length(BxCorrectorCurrents);
    BzZeroPos=find(BzCurrents==0);
    BxZeroPos=find(BxCurrents==0);
    if (isempty(BzZeroPos)==1||isempty(BxZeroPos)==1)
%         fprintf('BzCurrents should contain the ''zero'' value!\n')
%         return
        BxCurrentOrderedPoints=BxCurrents;
        BzCurrentOrderedPoints=BzCurrents;
    else
        if (BxZeroPos==1)
            BxCurrentOrderedPoints(1:NbBxCurrents)=BxCurrents(1:NbBxCurrents);
            BxCurrentOrderedPoints(NbBxCurrents+1:2*NbBxCurrents-1)=BxCurrents(NbBxCurrents-1:-1:1);
        else
            BxCurrentOrderedPoints(1:BxZeroPos)=BxCurrents(BxZeroPos:-1:1);
            BxCurrentOrderedPoints(BxZeroPos+1:BxZeroPos+NbBxCurrents-1)=BxCurrents(2:NbBxCurrents);
            BxCurrentOrderedPoints(BxZeroPos+NbBxCurrents:2*NbBxCurrents-2)=BxCurrents(NbBxCurrents-1:-1:BxZeroPos+1);            
        end
        if (BzZeroPos==1)
            BzCurrentOrderedPoints(1:NbBzCurrents)=BzCurrents(1:NbBzCurrents);
            BzCurrentOrderedPoints(NbBzCurrents+1:2*NbBzCurrents-1)=BzCurrents(NbBzCurrents-1:-1:1);
        else
            BzCurrentOrderedPoints(1:BzZeroPos)=BzCurrents(BzZeroPos:-1:1);
            BzCurrentOrderedPoints(BzZeroPos+1:BzZeroPos+NbBzCurrents-1)=BzCurrents(2:NbBzCurrents);
            BzCurrentOrderedPoints(BzZeroPos+NbBzCurrents:2*NbBzCurrents-1)=BzCurrents(NbBzCurrents-1:-1:BzZeroPos);
            
        end
    end
%     if (isempty(BxZeroPos)==1||isempty(BzZeroPos)==1)
%         fprintf('BxCurrents and BzCurrents should contain the ''zero'' value!\n')
%         return
%     else
%         if (BzZeroPos==1)
%             BzCurrentOrderedPoints(1:NbBzCurrents)=BzCurrents(1:NbBzCurrents);
%             BzCurrentOrderedPoints(NbBzCurrents+1:2*NbBzCurrents-1)=BzCurrents(NbBzCurrents-1:-1:1);
%         else
%             BzCurrentOrderedPoints(1:BzZeroPos)=BzCurrents(BzZeroPos:-1:1);
%             BzCurrentOrderedPoints(BzZeroPos+1:BzZeroPos+NbBzCurrents-1)=BzCurrents(2:NbBzCurrents);
%             BzCurrentOrderedPoints(BzZeroPos+NbBzCurrents:2*NbBzCurrents-1)=BzCurrents(NbBzCurrents-1:-1:BzZeroPos);
%             
%         end

        BzCurrentOrderedPoints
        BxCurrentOrderedPoints