function [res, TableCell]=idSummarizeUndulators(Types, SaveFile, Display)%, SortBy)
%% Written by F. Briquez 
% Modified 27/08/2018 : FTP added

%% 
    res=-1;
   
    fprintf ('\n\n=====================================================\n')
    fprintf ('                  Résumé onduleurs                   \n')
    fprintf ('=====================================================\n\n')
    fprintf (' --> Acquisition des données en cours...\n')
    
    NbUndulators=0;
    if strncmp(Types, 'Sort', 4)
        ListTypes={'InVac', 'WIGGLER', 'Apple2', 'EMPHU', 'EM'};
        NbTypes=length(ListTypes);
%         for iType=1:NbTypes
%             TempListOfUndulators=idGetListOfInsertionDevices(ListTypes{iType});
%             NbUndulators=NbUndulators+size(TempListOfUndulators, 1);
%         end
%        ListOfUndulators=cell(NbUndulators, 1};

        ListOfUndulators=idGetListOfInsertionDevices(ListTypes{1});
        for iType=2:NbTypes
            PreviousNbUndulators=size(ListOfUndulators, 1);
            TempListOfUndulators=idGetListOfInsertionDevices(ListTypes{iType});
            TempNbUndulators=size(TempListOfUndulators, 1);
            for i=1:TempNbUndulators
                for j=1:size(ListOfUndulators, 2)
                    ListOfUndulators{PreviousNbUndulators+i, j}=TempListOfUndulators{i, j};
                end
            end
        end
        
    else
        ListOfUndulators=idGetListOfInsertionDevices(Types);
    end
    
    NbUndulators=size(ListOfUndulators, 1);
    ListTitles={'Name', 'State', 'Gap', 'Phase', 'Offset', 'PS1', 'PS2', 'PS3', 'Backlash', 'Gap FB', 'Phase FB', 'FFWD', 'IgnoreFFerrors', 'Safety', 'Following'};
    TableCell=cell(NbUndulators+1, length(ListTitles));
    ListPossibleAttributes={'gap', 'phase', 'offset', 'currentPS1', 'currentPS2', 'currentPS3', 'currentBX1', 'currentBX2', 'currentBZP', 'currentIBP', 'backlashEnabled', 'gapPositionFeedback', 'phasePositionFeedback', 'lffOutputEnabled', 'ignoreAnyOfffError'};
    ListCorrespondingTitles={'Gap', 'Phase', 'Offset', 'PS1', 'PS2', 'PS3', 'PS1', 'PS2', 'PS3', 'PS1', 'Backlash', 'Gap FB', 'Phase FB', 'FFWD', 'IgnoreFFerrors'};
    
    %TableCell{:, :}='';
    for j=1:length(ListTitles)
        TableCell{1, j}=ListTitles{1, j};
    end
    for iUnd=1:NbUndulators
        idName=ListOfUndulators{iUnd, 1};
        
        if strncmp(idName, 'HU256', 5)
            fprintf(' ')
        end
        
        TableCell{iUnd+1, 1}=idName;
        
        DeviceServer=idGetUndDServer(idName);
        State=tango_state(DeviceServer);
        if ~isempty(State) && isstruct(State)
            StateString=State.name;
        else
            StateString='?';
        end
        TableCell{iUnd+1, 2}=StateString;
        
        CurrentListAttributes=tango_get_attribute_list(DeviceServer);
        
        for iPossibleAttribute=1:length(ListPossibleAttributes)
            PossibleAttribute=ListPossibleAttributes{iPossibleAttribute};
            Title=ListCorrespondingTitles{iPossibleAttribute};
            Column=FindStringInCell(Title, ListTitles);
            
            AttributeFound=FindStringInCell(PossibleAttribute, CurrentListAttributes);
            if AttributeFound~=-1   % the device has such an attribute 
                
                Attribute=tango_read_attribute(DeviceServer, PossibleAttribute);
                if isstruct(Attribute)
                    AttributeValue=Attribute.value(1);
                    if isnan(AttributeValue)
                        AttributeValue='-';
                    end
                else    % could not read?
                    AttributeValue='x';
                end
                
            else    % no such attribute for that undulator (or device not exported)
                if strcmp(StateString, '?') % Never appends : there is no cell of possible attributes...
                    AttributeValue='?';
                else
                    AttributeValue=' ';
                end
            end
            if strcmp(idName, 'HU640_DESIRS') && strcmp(Title, 'PS3') && AttributeFound~=-1
                fprintf ('row=%g, column=%g\n', iUnd+1, Column)
            end
            if isempty(TableCell{iUnd+1, Column})
                TableCell{iUnd+1, Column}=AttributeValue;
            end
            
        end
       
        Column=FindStringInCell('Safety', ListTitles);
        if Column~=-1
            Safety=idGetMotorizedInsertionInfo(idName, 'Safety');
             TableCell{iUnd+1, Column}=Safety;
        end
        Column=FindStringInCell('Following', ListTitles);
        if Column~=-1
            Safety=idGetMotorizedInsertionInfo(idName, 'ValidScanError');
             TableCell{iUnd+1, Column}=Safety;
        end
        
    end
        
    ColumnSeparator='  '; %'\t';
    if SaveFile
        FileName=sprintf ('Summary_%s_Undulators', Types);
        FileName=appendtimestamp(FileName);
        FileName=[FileName '.txt'];
        FileName=['/home/data/GMI/Points_Redemarrage/' FileName];
        PrintCellArrayOnScreeOrFile(TableCell, FileName, ColumnSeparator);
        fprintf ('Data saved in file ''%s''\n', FileName)
        
        succeeded=CopyFileFTP(FileName);
        if succeeded == -1
            fprintf ('Warning : File could not be duplicated in ''Passe-plat''\n')
        end
        
    end
        
    if Display
        PrintCellArrayOnScreeOrFile(TableCell, '', ColumnSeparator);
    end
    
    fprintf ('\n\n=====================================================\n')
    fprintf ('                   Résumé terminé                    \n')
    fprintf ('=====================================================\n\n')
   
    
    if ~SaveFile && ~Display
        res=TableCell;
    end
    
    return
    
end

function res=FindStringInCell(StringToFind, CellOfStrings)
    n=length(CellOfStrings);
    
    res=-1;
    
    if ~iscell(CellOfStrings)
        % fprintf('pas bon\n')
        return
    end
    
    for i=1:n
        if strcmp(StringToFind, CellOfStrings{i})
            res=i;
            return
        end
    end
    return
end

    
%% Display celle array on screen or in file
function res=PrintCellArrayOnScreeOrFile(TableCell, FileName, ColumnSeparator)
    res=-1;

    if (strcmp(FileName, '')==0)
        FileID=fopen(FileName, 'w+');
        if (FileID==-1)
            fprintf ('Could not open\n')
            return
        end
        fprintf (FileID, 'Undulator properties %s\n', date);
        res=fclose (FileID);
        if (res~=0)
            fprintf('Could not close\n')
            return
        end
        FileID=fopen(FileName, 'a');
        if (FileID==-1)
            fprintf ('Could not open\n')
            return
        end
    else
        fprintf('\n===============\n');
    end
    
   
    
%     NumberOfProperties=size(TableCell, 1);
%     NumberOfUndulators=size(TableCell, 2);
%     for i=1:NumberOfProperties
%         Line='';
%         for j=1:NumberOfUndulators
%             MaxLength=0;
%             for k=1:NumberOfProperties
%                 TempCell=TableCell{k, j};
%                 if (isscalar(TempCell))
%                     TempString=num2str(TempCell);
%                 elseif (ischar(TempCell))
%                     TempString=TempCell;
%                 end
%                 StrLength=length(TempString);
%                 if (StrLength>MaxLength)
%                     MaxLength=StrLength;
%                 end
%             end
%             TempCell=TableCell{i, j};
%             if (isscalar(TempCell))
%                 TempString=num2str(TempCell);
%             elseif (ischar(TempCell))
%                 TempString=TempCell;
%             end
%             if (length(TempString)<MaxLength)
%                 TempString=[TempString blanks(MaxLength-length(TempString))];
%             end

    TableCell=FormatCellArray(TableCell);
    for i=1:size(TableCell, 1)
        Line='';
        for j=1:size(TableCell, 2)
            TempString=TableCell{i, j};
            TempString=strjust(TempString, 'center');
            if (j~=1)
                Line=sprintf('%s%s%s', Line, ColumnSeparator, TempString);
            else
                Line=sprintf('%s%s', Line, TempString);
            end
        end
        if (strcmp(FileName, '')==0)
            fprintf (FileID, '%s\n', Line);
        else
            fprintf ('%s\n', Line);
        end
    end
    if (strcmp(FileName, '')==0)
        res=fclose(FileID);
        if (res~=0)
            fprintf('Could not close\n');
            return
        end
    end
    res=1;
    return
end

%% Create new cell with string fields of same length per each column
function NewCell=FormatCellArray(TableCell)
        
    NewCell=cell(size(TableCell));
    
    for j=1:size(TableCell, 2)
        MaxLength=0;
        for i=1:size(TableCell, 1)
            TempCell=TableCell{i, j};
            if (isscalar(TempCell))
                TempString=num2str(TempCell);
            elseif (ischar(TempCell))
                TempString=TempCell;
            end
            StrLength=length(TempString);
            if (StrLength>MaxLength)
                MaxLength=StrLength;
            end
        end
        for i=1:size(TableCell, 1)
        TempCell=TableCell{i, j};
            if isscalar(TempCell) || isempty(TempCell)
                TempString=num2str(TempCell);
            elseif (ischar(TempCell))
                TempString=TempCell;
            else
                TempString='';
            end
            if (length(TempString)<MaxLength)
                TempString=[TempString blanks(MaxLength-length(TempString))];
            end
            NewCell{i, j}=TempString;
        end
    end
    return
end

%% Sort a cell
function OutputCell=SortCellByColumns(InputCell, Row)
    OutputCell=cell(size(InputCell));
    for i=1:size(OutputCell, 1)
        OutputCell{i, 1}=InputCell{i, 1};
    end
    [~, Indx]=sort(InputCell(Row, 2:size(InputCell, 2)));
    
    for j=2:size(OutputCell, 2)
        NewColumn=Indx(j-1)+1;
        for i=1:size(OutputCell, 1)
            OutputCell{i, j}=InputCell{i, NewColumn};
        end
    end
end

%% Copy a file in ''Passe-plat''
function res=CopyFileFTP(fileName)
    
    res=-1;
    
    passePlatIP='172.17.22.58';
    passePlatUser='anonymous';
    passePlatPwd='';
    passePlatDirectory='TempOneWeek';
    insertionsDirectory='Insertions';
    passePlatFtp=ftp(passePlatIP, passePlatUser, passePlatPwd);
    
    fileFullPath=[filesep passePlatDirectory filesep insertionsDirectory filesep fileName];
    
    res=LookForDir(passePlatFtp, passePlatDirectory);
    if res==-1
        fprintf ('Could not copy file to FTP server : directory ''%s'' does not exist!\n', passePlatDirectory);
        return
    end
    cd (passePlatFtp, passePlatDirectory);
    
    res=LookForDir(passePlatFtp, insertionsDirectory);
     if res==-1
        mkdir(passePlatFtp, insertionsDirectory);
    end
    cd (passePlatFtp, insertionsDirectory);
    list=mput(passePlatFtp, fileName);
    if strcmp(list{1}, fileFullPath)
        res=0;
    end
end

%% Sub-function for FTP
function res=LookForDir(ftpObj, directory)
        res=-1;
        listStruct=dir(ftpObj);
    n=size(listStruct, 1);
    for i=1:n
        name=listStruct(i).name;
        isDir=listStruct(i).isdir;
        if (strcmp(name, directory) && isDir)
            break;
        end
    end
    if i==n
        return
    end
    res=0;
end