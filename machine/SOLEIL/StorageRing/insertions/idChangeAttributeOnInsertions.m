function res=idChangeAttributeOnInsertions(Attribute, Value, DisplayState, varargin)
    %% Read current attribute on several IDs and change the value if asked for.
    % Inputs :  - Attribute : attribute to change (string) (i.e.'gap')
    %           - Value : new value to set (string of number) (i.e. 5)
    %               if '', [], nan => no new value is set
    %           - DisplayState : display the state of the device in the
    %           result : 1 or 0
    %           - varargin :
    %               - one blank-separated string containing the types of
    %               undulators (i.e. 'Apple2 InVac')
    %               - one blank-separated string containing the types of
    %               undulators and one string containing the subdevice description (i.e. 'Apple2 InVac', 'offf')
    %               - one cell containing the ID names (i.e.
    %               {'HU52_DEIMOS', 'HU36_SIRIUS'}) or the device names
    %               (i.e. {'ans-c07/ei/m-hu52.1', 'ans-c15/ei/c-hu36'})
    %
    % Output : displays on screen the value of the attribute before (and
    % after setting the new setpoint) for each ID. 
    %
    % EXAMPLES FOR O. MARCOUILLE :
    % idChangeAttributeOnInsertions('lffOutputEnabled', 1, 1, 'mot');
    % idChangeAttributeOnInsertions('gap', 1, 1, 'mot');
    % idChangeAttributeOnInsertions('gap', 1, 1, 'Apple2');
    res=-1;
    
    timeToWaitAfterSucceed_s=1;
    timeToWaitBeforeRetry_s=2;
    maxNumberIterationsForRetry=3;
    
    cellPossibleResults={'succeeded', 'FAILED'};
    
    lengthResult=max(cellfun(@length, cellPossibleResults))+2;  
    
    cellOfUndNames='';
    cellOfDevices='';
    Types='';
    SubDevice='';
    
    if size(varargin, 2) == 1   % varargin is one cell or the Types string
        if iscell(varargin{1}) % varargin is the list of idNames or the list of devices
            inputCell=varargin{1};
            n=length(inputCell);
            tempCell=cell(1, n);
            for i=1:n
                 tempCell{i}=tango_info(inputCell{i});
            end
            %tempCell=cellfun(@tango_info, inputCell(:));
            tempVect=cellfun(@isstruct, tempCell);
            if ~isempty(find(tempVect)) % at least one element of varargin is a device name --> input is considered as the list of devices
                cellOfDevices=inputCell;
            else % input is the list of idNames
                cellOfUndNames=varargin{1};
                n=length(cellOfUndNames);
                for i=1:n
                    tempCell{i}=idGetUndDServer(cellOfUndNames{i});
                end
                tempVect=cellfun(@ischar, tempCell);
                if ~isempty(find(tempVect)) % at least one element is a device name
                    cellOfUndNames=varargin{1};
                    cellOfDevices=tempCell;
                else
                    fprintf ('idChangeAttributeOnInsertions : wrong input\n')
                end
            end
        else % varargin is Types
            if ischar(varargin{1})
                Types=varargin{1};
            else
                fprintf ('idChangeAttributeOnInsertions : wrong input\n')
                return
            end
        end       
        
    elseif size(varargin, 2) == 2 
        Types=varargin{1};
        SubDevice=varargin{2};
    else % varargin contains more than 2 elements
        fprintf ('idChangeAttributeOnInsertions : wrong input\n')
        return
    end
    
    if isempty(cellOfUndNames)
        if ~isempty(Types)
            ListOfUndulators=idGetListOfInsertionDevices(Types);
            cellOfUndNames=ListOfUndulators(:, 1);
            if ~isempty(SubDevice)
                for iUnd=1:length(cellOfUndNames)
                    idName=cellOfUndNames{iUnd};
                    DeviceServer=idGetUndDServer(idName);
                    if ~isempty(SubDevice)
                        if strcmpi(SubDevice, 'FFW') || strcmpi(SubDevice, 'OFFF')
                            DeviceServer=[DeviceServer '_OFFF'];
                        elseif strncmpi(SubDevice, 'Chan', 4)
                            DeviceServer=[DeviceServer '_' SubDevice];
                        end
                    end
                    cellOfDevices{iUnd}=DeviceServer;
                end
            end
        end
    end
    
    if isempty(cellOfDevices) 
        if isempty(cellOfUndNames)  % No way to create list of devices
            fprintf ('idChangeAttributeOnInsertions : wrong input\n')
            return
        else
            for iUnd=1:length(cellOfUndNames)
                cellOfDevices{iUnd}=idGetUndDServer(cellOfUndNames{iUnd});
            end
        end
    end
    
    if isempty(cellOfUndNames) % Devices are known but not ID names
        cellOfUndNames=cellOfDevices;
    end
    
    NbUndulators=length(cellOfDevices);
            
    cellOfResults=cell(NbUndulators, 1);
    cellOfValuesBefore=cell(NbUndulators, 1);
    cellOfValuesAfter=cell(NbUndulators, 1);
    cellOfStatesBefore=cell(NbUndulators, 1);
    cellOfStatesAfter=cell(NbUndulators, 1);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% First Loop to store values to display %%%%%%% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for iUnd=1:NbUndulators
        
        idName=cellOfUndNames{iUnd};
        DeviceServer=cellOfDevices{iUnd};
                
        %%%%%%% Read State %%%%%%% 
        StateBefore='';
        if DisplayState
            State=tango_state(DeviceServer);
            if isempty(State)
                StateBefore='x';
            elseif isnumeric(State)
                if State==-1
                    StateBefore='x';
                else
                    StateBefore='?';
                end
            elseif isstruct(State)
                    StateBefore=State.name;
            end
            StateBefore=[' (' StateBefore ') '];
        end
        cellOfStatesBefore{iUnd}=StateBefore;
        
        %%%%%%% Read attribute %%%%%%%
        if iUnd == 1
            tempStruct=tango_attribute_query(DeviceServer, Attribute);
            if ~isstruct(tempStruct)
                if tempStruct == -1
                    fprintf ('Attribute ''%s'' is incorrect for device ''%s''\n', Attribute, DeviceServer)
                    return
                end
            end
            
            %%%%%%% Check coherence between device attribute data type and 'Value' input type %%%%%%% 
            
            
            
            dataType=tempStruct.data_type;
            switch dataType

                case 5  % DevDouble
                    numeric=1;
                    if ~isempty(Value)
                        if ~isnumeric(Value)
                            Setpoint=str2double(Value);
                            if isnan(Setpoint)
                                fprintf ('Value should be a number for attribute ''%s'' of device ''%s''\n', Attribute, DeviceServer)
                                return
                            end
                        else
                            Setpoint=Value;
                        end
                    end

                case 1  % DevBoolean --> uint8
                    numeric=1;
                    if ~isempty(Value)
                        if ~isnumeric(Value)
                            Setpoint=str2double(Value);
                            if isnan(Setpoint)
                                fprintf ('Value should be a number for attribute ''%s'' of device ''%s''\n', Attribute, DeviceServer)
                                return
                            end
                        else
                            Setpoint=Value;
                        end
                        Setpoint=uint8(Setpoint);
                    end

                case 8  % DevString --> num2str
                    numeric=0;
                    if ~isempty(Value)
                        if isnumeric(Value)
                            Setpoint=num2str(Value);
                        end
                    end

                case 19 % State --> ?
                    numeric=0;

                case 6  % DevUShort --> uint16
                    numeric=1;
                    if ~isempty(Value)
                        if ~isnumeric(Value)
                            Setpoint=str2double(Value);
                            if isnan(Setpoint)
                                fprintf ('Value should be a number for attribute ''%s'' of device ''%s''\n', Attribute, DeviceServer)
                                return
                            end
                        else
                            Setpoint=Value;
                        end
                        Setpoint=uint16(Setpoint);
                    end

                case 2  % DevShort --> int16
                    numeric=1;
                    if ~isempty(Value)
                        if ~isnumeric(Value)
                            Setpoint=str2double(Value);
                            if isnan(Setpoint)
                                fprintf ('Value should be a number for attribute ''%s'' of device ''%s''\n', Attribute, DeviceServer)
                                return
                            end
                        else
                            Setpoint=Value;
                        end
                        Setpoint=int16(Setpoint);
                    end

                case 4   % Float --> single
                    numeric=1;
                    if ~isempty(Value)
                        if ~isnumeric(Value)
                            Setpoint=str2double(Value);
                            if isnan(Setpoint)
                                fprintf ('Value should be a number for attribute ''%s'' of device ''%s''\n', Attribute, DeviceServer)
                                return
                            end
                        else
                            Setpoint=Value;
                        end
                        Setpoint=single(Setpoint);
                    end

                otherwise
                    fprintf ('Unknown type for attribute ''%s'' of device ''%s''\n', Attribute, DeviceServer)
                    return
                
            end
        end
        
        %%%%%%% Read attribute %%%%%%% 
        AttributeBefore=ReadAttribute(DeviceServer, Attribute, numeric);
        cellOfValuesBefore{iUnd}=AttributeBefore;

        % Check if it is asked to write a new value
        if ~isempty(Value) && ~isnan(Value) % Writing a new value is asked
            Write=1;
        else
            Write=0;
        end

        %%%%%%% Write new value %%%%%%% 
        if Write % It is asked to write a new value
            i=1;
            continueCondition=1;
            while (continueCondition)   % loop of several tries

                % Write the value
                tango_write_attribute(DeviceServer, Attribute, Setpoint);
                if tango_error==-1
                    resString=cellPossibleResults{2}; % Failed
                    i=i+1;
                    continueCondition = (i<maxNumberIterationsForRetry);
                    if continueCondition
                        pause (timeToWaitBeforeRetry_s);
                    end
                else
                    resString=cellPossibleResults{1}; % Succeeded
                    pause (timeToWaitAfterSucceed_s);
                    continueCondition=0;
                end
            end
            cellOfResults{iUnd}=resString;

            %%%%%%% Read new State %%%%%%% 
            StateAfter='';
            if DisplayState
                State=tango_state(DeviceServer);
                 if isempty(State)
                    StateAfter='x';
                elseif isnumeric(State)
                    if State==-1
                        StateAfter='x';
                    else
                        StateAfter='?';
                    end
                elseif isstruct(State)
                        StateAfter=State.name;
                 end
                 StateAfter=[' (' StateAfter ') '];           
            end
            cellOfStatesAfter{iUnd}=StateAfter;

            %%%%%%%  Read the new value of attribute %%%%%%% 
             AttributeAfter=ReadAttribute(DeviceServer, Attribute, numeric);
             cellOfValuesAfter{iUnd}=AttributeAfter;
        end
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%% Compute widths of columns for text formatting %%%%%%% 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    lengthUndNames=max(cellfun(@length, cellOfUndNames))+2;
    lengthAttributeBefore=max(cellfun(@length, cellOfValuesBefore)+2);
    lengthStatesBefore=max(cellfun(@length,cellOfStatesBefore)+2);    
    if Write
        lengthResult=max(cellfun(@length, cellOfResults))+2;
        lengthAttributeAfter=max(cellfun(@length, cellOfValuesAfter))+2;
        lengthStatesAfter=max(cellfun(@length,cellOfStatesAfter)+2);
    end
    

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%% Second Loop to format and diplay data stored in first Loop %%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for iUnd=1:NbUndulators
        
        idName=cellOfUndNames{iUnd};
        AttributeBefore=cellOfValuesBefore{iUnd};
        stringIdName=increaseString([idName ':'], lengthUndNames, 'l');
        
        if numeric
            stringBefore=increaseString(num2str(AttributeBefore), lengthAttributeBefore, 'l');
        else
            stringBefore=increaseString(AttributeBefore, lengthAttributeBefore, 'l');
        end
        
        if DisplayState
            StateBefore=cellOfStatesBefore{iUnd};
            StateBefore=increaseString(StateBefore, lengthStatesBefore, 'l');
        else
            StateBefore='';
        end
        
        if Write
            resString=cellOfResults{iUnd};
            stringRes=increaseString(resString, lengthResult, 'l');
            AttributeAfter=cellOfValuesAfter{iUnd};
            if numeric
                stringAfter=increaseString(num2str(AttributeAfter), lengthAttributeAfter, 'l');
            else
                stringAfter=increaseString(AttributeAfter, lengthAttributeAfter, 'l');
            end
            
            if DisplayState
                StateAfter=cellOfStatesAfter{iUnd};
                StateAfter=increaseString(StateAfter, lengthStatesAfter, 'l');
            else
                StateAfter='';
            end
        end
       
        if ~Write
            fprintf ('%s\t %s = %s%s\n', stringIdName, Attribute, stringBefore, StateBefore);
        else
            fprintf ('%s\t %s %s :\t %s%s --> %s%s\n', stringIdName, Attribute, stringRes, stringBefore, StateBefore, stringAfter, StateAfter);
        end        
        res=0;
    end
    return
    
end

function Value=ReadAttribute(DeviceServer, Attribute, Numeric1_String0)
    Structure=tango_read_attribute(DeviceServer, Attribute);
    if isnumeric(Structure)
        if Structure==-1
            if Numeric1_String0
                Value=nan;
            else
                Value='x';
            end
        end
    else
        if Numeric1_String0
            Field=Structure.value;
            Value=Field(1);
        else
            Value=Structure.name;
        end
    end
    return
end

function res=increaseString(inputString, n, justify)
    if findstr('l', justify)
        justify='left';
    elseif findstr('r', justify)
        justify='right';
    elseif findstr('c', justify)
        justify='center';
    else
        justify='';
    end
    outputString=sprintf('%*s', n, inputString);
    if ~isempty(justify)
        res=strjust(outputString, justify);
    end
    return
end
