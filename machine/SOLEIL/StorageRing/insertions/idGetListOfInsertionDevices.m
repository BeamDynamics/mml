function res=idGetListOfInsertionDevices(Types)
% idGetListOfInsertionDevices - Returns a (nx3) cell array containing : 
%Alias of idGetParamForUndSOLEIL;
%  OUTPUTS
%  1. a cell array
%       - undulator name
%       - Storage Ring cell number
%       - straight section type

% Types can be :    - a string containing elements separated by blanks (not case-sensitive) :
% 'InVac', 'Apple2', 'EM', 'Wiggler', 'EMPHU'
%                   - 'all' => all IDs (even those unknown or empty Straight Sections)
%                   - ''    => usual types : 'InVac', 'Apple2' and 'EM'
%                   (without out-vacuum Wiggler and EMPHU)
%                   - 'mot' => motorizedd IDs (equivalent to 'Apple2 InVac
%                   Wiggler EMPHU')
%                   - 'gmid' => all IDs with a device server of class GMID
%                   (equivalent to 'Apple2 InVac Wiggler')
%                   Wiggler EMPHU'

%
%% Written by A.Bence
    
    res2=idGetParamForUndSOLEIL(Types);
    if isempty(res2)
        res=-1;
    else
        res(:,1)={res2.name};
        res(:,2)={res2.cell};
        res(:,3)={res2.straight};
        res(:,4)={res2.type};
    end
