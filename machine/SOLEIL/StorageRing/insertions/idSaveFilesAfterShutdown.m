function idSaveFilesAfterShutdown(attributes, properties)
    if attributes~=0
        idSummarizeUndulators('Sort', attributes>1, 1);
    end
    if properties
        idGetTableOfMotorizedInsertionProperties2(properties>1, 'Motorized', 1, 1);
        %     idGetTableOfMotorizedInsertionProperties2(1, 'all', 1, 0);
    end
end