function res=idApplyCommandOnInsertions(Command, Types, SubDevice)
    %%
    % EXAMPLES FOR O. MARCOUILLE :
%     idApplyCommandOnInsertions('On','mot','chan1'); --> met ON tous les
%     CHAN1 des onduleurs motorisés (Apple2, Sous-vide, Wiggler et EMPHU
%     (qui n'en a pas!)
%     idApplyCommandOnInsertions('On','mot','chan2');
%     idApplyCommandOnInsertions('On','mot','chan3');
%     idApplyCommandOnInsertions('On','mot','chan4');
%     idApplyCommandOnInsertions('Init','mot','offf'); --> 
%     idApplyCommandOnInsertions('On','mot','offf');
% 
%     idApplyCommandOnInsertions('','mot','offf');
%     idApplyCommandOnInsertions('','mot','chan1');
%     idApplyCommandOnInsertions('','mot','chan2');
%     idApplyCommandOnInsertions('','mot','chan3');
%     idApplyCommandOnInsertions('','mot','chan4');

    res=-1;
    
    DisplayState=1;
    
    timeToWaitAfterSucceed_s=1;
    timeToWaitBeforeRetry_s=2;
    maxNumberIterationsForRetry=3;
    lengthUndNames=20;
    lengthResult=10;
    lengthState=6;
    ListOfUndulators=idGetListOfInsertionDevices(Types);
    
    NbUndulators=size(ListOfUndulators, 1);
       
    for iUnd=1:NbUndulators
        idName=ListOfUndulators{iUnd, 1};
              
        DeviceServer=idGetUndDServer(idName);
        %%DeviceServer
        if ~isempty(SubDevice)
            if strcmpi(SubDevice, 'FFW') || strcmpi(SubDevice, 'OFFF')
                DeviceServer=[DeviceServer '_OFFF'];
            else
                DeviceServer=[DeviceServer '_' SubDevice];
            end
        end
      
        StateBefore='';
        if DisplayState
            State=tango_state(DeviceServer);
            if isempty(State)
                StateBefore='x';
            elseif isnumeric(State)
                if State==-1
                    StateBefore='x';
                else
                    StateBefore='?';
                end
            elseif isstruct(State)
                    StateBefore=State.name;
            end
            StateBefore=['(' StateBefore ')'];
        end
        
        if ~isempty(Command)
        
            i=1;
            continueCondition=1;
            while (continueCondition)
                resValue=tango_command_inout(DeviceServer, Command);

                if resValue==-1
                    resString='FAILED';
                    i=i+1;
                    continueCondition = (i<maxNumberIterationsForRetry);
                    if continueCondition
                        pause (timeToWaitBeforeRetry_s);
                    end
                else
                    resString='succeeded';
                    pause (timeToWaitAfterSucceed_s);
                    continueCondition=0;
                end
            end

            StateAfter='';
            if DisplayState
                State=tango_state(DeviceServer);
                 if isempty(State)
                    StateAfter='x';
                elseif isnumeric(State)
                    if State==-1
                        StateAfter='x';
                    else
                        StateAfter='?';
                    end
                elseif isstruct(State)
                        StateAfter=State.name;
                 end
                 StateAfter=['(' StateAfter ')'];           
            end
        
            fprintf ('%s\t %s %s :\t %s --> %s\n', increaseString([idName ':'], lengthUndNames, 'l'), Command, increaseString(resString, lengthResult, 'l'), increaseString(StateBefore, lengthState, 'l'), increaseString(StateAfter, lengthState, 'l'));
        
        else
            
            fprintf ('%s\t %s\n', increaseString([idName ':'], lengthUndNames, 'l'), increaseString(StateBefore, lengthState, 'l'));
            
        end
        res=0;
    end
    return
end

function res=increaseString(inputString, n, justify)
    if findstr('l', justify)
        justify='left';
    elseif findstr('r', justify)
        justify='right';
    elseif findstr('c', justify)
        justify='center';
    else
        justify='';
    end
    outputString=sprintf('%*s', n, inputString);
    if ~isempty(justify)
        res=strjust(outputString, justify);
    end
    return
end