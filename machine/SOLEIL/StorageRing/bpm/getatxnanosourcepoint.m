function getatxnanosourcepoint
%% getatxnanosourcepoint --  get source point from TANGO of Nanoscopium et
%% ATX beamlines


% fprintf('\n---------------- REFERENCE 2014 ------------\n');
% fprintf('ANATOMIX SDL13 upstream\n')
% fprintf('H-Position = %+6.1f um H-angle %+6.2f µrad\n', ...
%     -0.507, +0.00);
% fprintf('V-Position = %+6.1f um V-angle %+6.2f µrad \n', ...
%     -18.02, -8.39);
% 
% fprintf('NANOSCOPIUM SDL13 downstream \n')
% fprintf('H-Position = %+6.1f um H-angle %+6.2f µrad\n', ...
%     -13.012, +1.58);
% fprintf('V-Position = %+6.1f um V-angle %+6.2f µrad \n', ...
%     -102.475, -37.51);
% fprintf('---------------------------------------------\n\n');
% 

Ibeam = getdcct;

XBPMATXDeviceName = 'TDL-I13-LT/DG/XBPM_LIB.1';

ATX.X = readattribute([XBPMATXDeviceName '/XPosSA'])*1e3; % um
ATX.Z = readattribute([XBPMATXDeviceName '/ZPosSA'])*1e3; % um

XBPMNANODeviceName = 'TDL-I13-LN/DG/XBPM_LIB.1';

NANO.X = readattribute([XBPMNANODeviceName '/XPosSA'])*1e3; % um
NANO.Z = readattribute([XBPMNANODeviceName '/ZPosSA'])*1e3; % um


U18ATXDeviceName ='ANS-C13/EI/L-U18.ATX';
ATX.gap = readattribute([U18ATXDeviceName '/gap']);

U18NANODeviceName ='ANS-C13/EI/L-U18.NANO';
NANO.gap = readattribute([U18NANODeviceName '/gap']);

TimeStamp = datestr(clock) ;
fprintf('\n------------MEASURE %s (Reference 2014; gap values @5.5 mm)-----------\n', TimeStamp);
fprintf('Stored beam %+6.1f mA \n', Ibeam);
cprintf('blue', 'ANATOMIX SDL13 upstream\n')
fprintf('Gap U18: %+6.2f mm, XBPM X = %+6.1f µm (xxx µm @500 mA) Z = %+6.1f µm (xxx µm @500 mA) \n', ...
    ATX.gap, ATX.X, ATX.Z);
fprintf('H-Position = %+6.1f (%+6.1f) µm H-angle %+6.1f (%+6.1f) µrad\n', ...
    getpv('BeamLine', 'Positionx', [13 1]), -0.507, getpv('BeamLine', 'Anglex', [13 1]), +0.00);
fprintf('V-Position = %+6.1f (%+6.1f) µm V-angle %+6.1f (%+6.1f) µrad \n', ...
    getpv('BeamLine', 'Positionz', [13 1]), -18.02, getpv('BeamLine', 'Anglez', [13 1]), -8.39);

cprintf('blue', 'NANOSCOPIUM SDL13 downstream \n')
fprintf('Gap U18: %+6.2f mm, XBPM X = %+6.1f µm (xxx µm @500 mA)  Z = %+6.1f µm (xxx µm @500 mA) \n', NANO.gap, NANO.X, NANO.Z);
fprintf('H-Position = %+6.1f (%+6.1f) µm H-angle %+6.1f (%+6.1f) µrad\n', ...
    getpv('BeamLine', 'Positionx', [13 2]), -13.012, getpv('BeamLine', 'Anglex', [13 2]), +1.58);
fprintf('V-Position = %+6.1f (%+6.1f) µm V-angle %+6.1f (%+6.1f) µrad \n', ...
    getpv('BeamLine', 'Positionz', [13 2]), -102.475, getpv('BeamLine', 'Anglez', [13 2]), -37.51);

