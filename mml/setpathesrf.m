function [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathesrf(varargin)
%SETPATHSOLEIL Initializes the Matlab Middle Layer (MML) for ESRF
%  [MachineName, SubMachineName, OnlineLinkMethod, MMLROOT] = setpathsoleil(SubMachineName)
%
%  INPUTS
%  1. SubMachineName - 'StorageRing', 'Booster', 'TL2', 'TL1'
%
%  NOTES
%  1. calls aoinit that call [machine]init
%
%  See also setoperationalmode, setmmldirectories, aoinit, soleilinit

%
%  Written by Gregory J. Portmann
%  Adapted by Laurent S. Nadolski

Machine = 'ESRF';

%%%%%%%%%%%%%%%%%
% Input Parsing %
%%%%%%%%%%%%%%%%%

% First strip-out the link method (although it should not be there)
LinkFlag = '';
for i = length(varargin):-1:1
    if ~ischar(varargin{i})
        % Ignore input
    elseif strcmpi(varargin{i},'LabCA')
        LinkFlag = 'LabCA';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'MCA')
        LinkFlag = 'MCA';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'SCA')
        LinkFlag = 'SCA';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'SLC')
        LinkFlag = 'SLC';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'Tango')
        LinkFlag = 'Tango';
        varargin(i) = [];
    elseif strcmpi(varargin{i},'UCODE')
        LinkFlag = 'UCODE';
        varargin(i) = [];
    end
end

if isempty(LinkFlag)
    LinkFlag = 'Tango';
end


% Get the submachine name
if length(varargin) >= 1
    SubMachineName = varargin{1};
else
    SubMachineName = 'StorageRing';
end

if isempty(SubMachineName)
    SubMachineNameCell = {'TL1', 'Booster', 'TL2', 'Storage Ring'};
    [i, ok] = listdlg('PromptString', 'Select an accelerator:',...
        'SelectionMode', 'Single',...
        'Name', 'ESRF', ...
        'ListString', SubMachineNameCell,'ListSize', [160 60]);
    if ok
        SubMachineName = SubMachineNameCell{i};
    else
        fprintf('Initialization cancelled (no path change).\n');
        return;
    end
end

if any(strcmpi(SubMachineName, {'Storage Ring','Ring'}))
    SubMachineName = 'StorageRing';
end

% Common path at ESRF
   try
       MMLROOT = getmmlroot;
       fprintf('%s\n', MMLROOT);
    
       [~, servername] = system('uname -n');
       
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common'));
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'archiving'));
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'database'));
       addpath(fullfile(MMLROOT, 'mml', 'plotfamily')); % Greg's version
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'plotfamily'));
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'configurations'));
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'cycling'));
       addpath(fullfile(MMLROOT, 'machine', 'SOLEIL', 'common', 'diag', 'DserverBPM'));
       addpath(fullfile(MMLROOT, 'applications', 'mmlviewer'));
       % GUILayout
       addpath(fullfile(ToolboxPath,'GUILayout'));
       addpath(fullfile(ToolboxPath,'GUILayout/Patch'));
       addpath(fullfile(ToolboxPath,'GUILayout/layoutHelp'));
       % Fundamental Physical constants NIST
       %addpath(fullfile(ToolboxPath,'fundamentalPhysicalConstantsFromNIST'));
       % Yair Altman Undocumented Secrets of Matlab
       addpath(fullfile(ToolboxPath,'cprintf'));       
       addpath(fullfile(ToolboxPath,'uiinspect')); 
       addpath(fullfile(ToolboxPath,'findjobj')); 
       addpath(fullfile(ToolboxPath,'setPrompt')); 
       addpath(fullfile(ToolboxPath,'tabcomplete')); 
       addpath(fullfile(ToolboxPath,'EditorMacro')); 
       addpath(fullfile(ToolboxPath,'getundoc')); 
       addpath(fullfile(ToolboxPath,'statusbar'));
       addpath(fullfile(ToolboxPath,'createTable'));
       addpath(fullfile(ToolboxPath,'others'));
       addpath(fullfile(ToolboxPath,'export_fig'));
   catch errRecord
       fprintf('Path loading failed %s\n', errRecord.message);
   end
   
   
if strcmpi(SubMachineName,'StorageRing')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'StorageRing', 'StorageRing', LinkFlag);
elseif strcmpi(SubMachineName,'TL1')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'TL1',         'Transport',   LinkFlag);
elseif strcmpi(SubMachineName,'Booster')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'Booster',     'Booster',     LinkFlag);
elseif strcmpi(SubMachineName,'TL2')
    [MachineName, SubMachineName, LinkFlag, MMLROOT] = setpathmml(Machine, 'TL2',         'Transport',   LinkFlag);
else
    error('SubMachineName %s unknown', SubMachineName);
end