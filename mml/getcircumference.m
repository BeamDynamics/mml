function C = getcircumference
%GETCIRCUMFERENCE - Returns the ring circumference
%   C = getcircumference
%
%  OUTPUTS
%  1. C - ring circumference in meters
%
%  See Also getenergy, getmcf, getbrho

%
% Written by Laurent S. Nadolski

C = getfamilydata('Circumference');
